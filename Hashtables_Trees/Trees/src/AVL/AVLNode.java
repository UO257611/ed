package AVL;

public class AVLNode<T> 
{
	private T element;
	private AVLNode<T> left;
	private AVLNode<T> right;
	private int height; 
	
	
	public AVLNode(T x)
	{
		this(x, null, null,0);
	}
	
	public AVLNode(T x, AVLNode<T> le,AVLNode<T> ri,int he )
	{
		setElement(x);
		setRight(ri);
		setLeft(le);
		setHeight(he);
	}
	
	
	public String toString() 
	{ 
		//return getElement().toString(); 
		//return getElement().toString() + "(" + getHeight() + ")";
		return getElement().toString() + "(" + getBF() + ")";
	}
	
	public void print() 
	{ 
		System.out.println(toString());
	}

	
	public T getElement() 
	{
		return element;
	}
	public void setElement(T element) 
	{
		this.element = element;
	}
	public AVLNode<T> getLeft()
	{
		return left;
	}
	public void setLeft(AVLNode<T> left)
	{
		this.left = left;
	}
	public AVLNode<T> getRight()
	{
		return right;
	}
	public void setRight(AVLNode<T> right) 
	{
		this.right = right;
	}
	
	
	public void updateHeight()
	{
		if(getLeft()==null && getRight()==null)
		{
			setHeight(0);
		}
		else
		{
			if(getLeft()==null)
			{
				height = 1 + getRight().getHeight(); 
			}
			else
			{
				if(getRight()==null)
				{
					height = 1 + getLeft().getHeight(); 
				}
				else
				{
					if(getRight().height >= getLeft().height)
					{
						height = 1 + getRight().getHeight(); 
					}
					else
					{
						height = 1 + getLeft().getHeight(); 
					}
				}
			}
		}
		
	}

	public void setHeight(int i) {
		this.height=i;
		
	}

	public int getHeight() {
		return height;
	}
	
	
	
	public int getBF()
	{
		if(getLeft()==null && getRight()==null)
		{
			return 0;
		}
		
		else
		{
			 if(getRight()==null)
			 {
				 return - (1+ getLeft().getHeight()) ;
			 }
			 else
			 {
				 if(getLeft()==null)
				 {
					 return getRight().getHeight()+1;
				 }
				 else
				 {
					 return getRight().getHeight() - getLeft().getHeight();
				}
			 }
		}
		
		
		
		
	}
	
}
