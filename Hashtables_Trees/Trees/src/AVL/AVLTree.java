package AVL;

public class AVLTree<T extends Comparable<T>> { 


	private AVLNode<T> root;
	
	public AVLTree()
	{
		root= null;
	}

	public AVLNode<T> getRoot() 
	{
		return root;
	}

	public void setRoot(AVLNode<T> root) 
	{
		this.root = root;
	}
	
	public void add(T element) throws Exception
	{
		root = add( root, element);
	}

	private AVLNode<T> add(AVLNode<T> theRoot, T element) throws Exception {
		if(theRoot == null)
		{
			theRoot= new AVLNode(element);
		}
		else
		{
			if(element.compareTo(theRoot.getElement())==0)
			{
				throw new Exception("There is a repeated node");
			}
			else
			{
				if(element.compareTo(theRoot.getElement())<0)
				{
					theRoot.setLeft( add(theRoot.getLeft(), element) );
				}
				else if(element.compareTo(theRoot.getElement())>0)
				{
					theRoot.setRight(add(theRoot.getRight(), element));	
				}
			}
		
			
		}
		//theRoot.updateHeight();
		return updateBF(theRoot);
		//return theRoot;
		

	}
	
	
	
	public String toString()
	{
		
		return toString(root);
		
	}

	private String toString(AVLNode<T> theRoot) {
		
		
		if(theRoot == null)
		{
			return "-";
		}
		else 
		{
		
		return theRoot.toString()+ 
				toString(theRoot.getLeft())+  
				 toString(theRoot.getRight());
		}
	}
	
	
	public boolean search(T element)
	{
		
		
		return search(element, root);
		
		
		
		
		
	}

	private boolean search(T element, AVLNode<T> theRoot) 
	{
		if(theRoot == null)
		{
			return false;
		}
		
		if(theRoot.getElement().compareTo(element) == 0)
		{
			return true;
		}
		else if( theRoot.getElement().compareTo(element)<0)
		{
			return search(element, theRoot.getRight());
		}
		else if (theRoot.getElement().compareTo(element)>0)
		{
			return search(element, theRoot.getLeft());
		
		}
		
		return false;
	}
	
	
	public T getMax(AVLNode<T> yroot)
	{
		while( yroot.getRight()!=null)
		{
			yroot =yroot.getRight();
		}
		return yroot.getElement();
		
	}
	
	
	
	
	//////////////////////////////////
	//////////////////////////////////
	/////BINARY TREE SEARCH METHOD////
	//////////////////////////////////
	//////////////////////////////////
	
	
	public void remove(T element) throws Exception
	{
		root= remove(root, element);
		
		
		
	}

	private AVLNode<T> remove(AVLNode<T> theRoot, T element) throws Exception 
	{
		
		if(theRoot== null)
		{
		throw new Exception("");	
		}
		else
		{
			if(element.compareTo(theRoot.getElement()) < 0) 
			{
				 theRoot.setLeft(remove(theRoot.getLeft(), element));
			}
			else
			{
				if(element.compareTo(theRoot.getElement()) > 0)
					{
						
					theRoot.setRight(remove(theRoot.getRight(), element));
					
					}
				else
				{
					if(theRoot.getLeft()== null)
					{
						return theRoot.getRight();
					}
					else
					{
						if(theRoot.getRight()==null)
						{
							return theRoot.getLeft();
						}
						else
						{
							theRoot.setElement(getMax(theRoot.getLeft()));
							theRoot.setLeft(remove(theRoot.getLeft(),getMax(theRoot.getLeft())));
						}
					}
				}
			}
			
		}
		
		//theRoot.updateHeight();
		return updateBF(theRoot);
		//return theRoot;
		
		
	}

	public AVLTree<T> joinsNonRecursive(AVLTree<T> tree) throws Exception 
	{
		AVLTree<T> x = new AVLTree<T>();
		AVLNode<T>theRoot= root;
		x.setRoot(root);
		while(tree.getRoot()!=null)
		{
			if(x.search(tree.getRoot().getElement()))
					{
				    	tree.remove(tree.getRoot().getElement());
				    	continue;
					}
			x.add(tree.getRoot().getElement());
			tree.remove(tree.getRoot().getElement());
			
		}
		
	return x ;	
		
	}
	
	
	
	public AVLTree<T> joins(AVLTree<T> tree) 
	{
		AVLTree<T> theTree = new AVLTree<T>();
		
		joins(root, theTree);
		joins(tree.getRoot(), theTree);
		
		return theTree;
		
		
	}

	private void joins(AVLNode<T> theRoot, AVLTree<T> theTree) 
	{
		if(theRoot!= null)
		{
				try 
				{
					theTree.add(theRoot.getElement());
				} 
				catch (Exception e) 
				{
					
				}
		
		
		joins(theRoot.getLeft(), theTree);
		joins(theRoot.getRight(), theTree);
		}
		
					
		
		}
	
	
	private AVLNode<T> updateBF(AVLNode<T> theRoot) 
	{ 
		if(theRoot.getBF() == -2) 
		{  
			// Left rotation // Does the AVL need single or double rotation?
			if(theRoot.getLeft().getBF() <=0)
			{
				theRoot= singleLeftRotation(theRoot);
			}
			else
			{
				theRoot= doubleLeftRotation(theRoot);
				
			}
		} 
		else if(theRoot.getBF() == 2) 
		{ 
			// Right rotation // Does the AVL need single or double rotation?
			if(theRoot.getRight().getBF() >= 0)
			{
				theRoot= singleRightRotation(theRoot);
			}
			else
			{
				theRoot= doubleRightRotation(theRoot);
				
			}
			
		} 
		
		theRoot.updateHeight(); 
		return theRoot; 
		
	}
	
	
	
	
	
	private AVLNode<T> singleLeftRotation(AVLNode<T> b)
	{
		AVLNode<T> a = b.getLeft();
		
		b.setLeft(a.getRight());
		
	
		a.setRight(b);
		a.updateHeight();
		b.updateHeight();
		return a;
		
	}
	
	


private AVLNode<T> singleRightRotation(AVLNode<T> b)
{
	AVLNode<T> a = b.getRight();
	
	b.setRight(a.getLeft());
	

	a.setLeft(b);
	
	a.updateHeight();
	b.updateHeight();
	
	return a;
	
}


private AVLNode<T> doubleLeftRotation(AVLNode<T> c) 
{
	AVLNode<T> a = c.getLeft();
	AVLNode<T> b = a.getRight();
	
	a.setRight(b.getLeft());
	c.setLeft(b.getRight());
	
	
	b.setLeft(a);
	b.setRight(c);
	
	a.updateHeight();
	c.updateHeight();
	b.updateHeight();
	
	return b;
}

private AVLNode<T> doubleRightRotation(AVLNode<T> c) 
{
	AVLNode<T> a = c.getRight();
	AVLNode<T> b = a.getLeft();
	
	a.setLeft(b.getRight());
	c.setRight(b.getLeft());
	
	
	b.setLeft(c);
	b.setRight(a);
	
	a.updateHeight();
	c.updateHeight();
	b.updateHeight();
	
	return b;
}



public int getHeight()
{
	if(root!= null)
	{
		return getHeight(root, 1);
	}
	else
	{
		return 0;
	}
}

private int getHeight(AVLNode<T> theRoot, int counter) 
{
	int maxCounter1 = 0;

	int maxCounter2 = 0;
	
	if(theRoot.getLeft() == null && theRoot.getRight() == null)
	{
		return counter;
		
	}
	//
	if(theRoot.getLeft() != null)
	{
		int x = counter + 1;
		maxCounter1 = getHeight(theRoot.getLeft(),x );
		
	}
	if(theRoot.getRight() != null)
	{
		int x = counter + 1;
		maxCounter2 = getHeight(theRoot.getRight(), x);
	
	}
	
	if(maxCounter1 < maxCounter2)
	{
		return maxCounter2;
	}
	else
	{
		return maxCounter1
				;
	}
	
	
	
	
	
	
	
	
	
	
}


}
	
	
	
	

	
	
	

	
	
	
	
	
	
	
	
	
	
	
	


