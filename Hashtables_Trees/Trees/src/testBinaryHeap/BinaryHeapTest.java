package testBinaryHeap;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import BinaryHeaps.BinaryHeap;

public class BinaryHeapTest {

	@Before
	public void setUp() throws Exception 
	{
		
		
	}
	
	
	
	@Test
	public void constructorTest()
	{
		Integer[] z ={10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
		BinaryHeap<Integer> a = new BinaryHeap<Integer>(z);
		assertEquals("[1, 2, 4, 3, 6, 5, 8, 10, 7, 9]",a.toString());
		
		Integer[] b = { 20, 2, 4, 1};
		
		BinaryHeap<Integer> x = new BinaryHeap<Integer>(b);
		assertEquals("[1, 2, 4, 20]",x.toString());
		
		Integer[] c = { 1,7,4,10,8};
		
		BinaryHeap<Integer> y = new BinaryHeap<Integer>(c);
		assertEquals("[1, 7, 4, 10, 8]",y.toString());
		
		Integer[] d= { 1,7,-4,10,-8};
		
		BinaryHeap<Integer> t = new BinaryHeap<Integer>(d);
		assertEquals("[-8, 1, -4, 10, 7]",t.toString());
		
		
	}
	
	@Test
	public void addTest()
	{
		BinaryHeap<Integer> t = new BinaryHeap<Integer>();
		assertEquals("[]",t.toString());
		
		t.add(1);
		t.add(-1);
		assertEquals("[-1, 1]",t.toString());
		
		t.add(2);
		t.add(5);
		assertEquals("[-1, 1, 2, 5]",t.toString());
		
		t.add(0);
		assertEquals("[-1, 0, 2, 5, 1]",t.toString());
		
		t.add(null);
		assertEquals("[-1, 0, 2, 5, 1]",t.toString());
	}
	
	@Test
	public void getMinTest()
	{
		Integer[] z ={10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
		BinaryHeap<Integer> a = new BinaryHeap<Integer>(z);
		assertEquals("[1, 2, 4, 3, 6, 5, 8, 10, 7, 9]",a.toString());
		
		assertEquals((Integer)1, a.getMin());
		assertEquals("[2, 3, 4, 7, 6, 5, 8, 10, 9]",a.toString());
		
	}
	
	
	
	
	
	

}
