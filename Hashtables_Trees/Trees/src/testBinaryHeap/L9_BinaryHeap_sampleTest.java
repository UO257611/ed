package testBinaryHeap;
import static org.junit.Assert.*;
import org.junit.Test;

import BinaryHeaps.BinaryHeap;

public class L9_BinaryHeap_sampleTest {
    
    @Test
    public void test_A() {
        BinaryHeap<Integer> a = new BinaryHeap<Integer>();
        
        a.add(10);
        a.add(9);
        a.add(8);
        assertEquals("[8, 10, 9]", a.toString());
        
        a.add(7);
        assertEquals("[7, 8, 9, 10]", a.toString());
        
        a.add(6);
        assertEquals("[6, 7, 9, 10, 8]", a.toString());
        
        a.add(5);
        assertEquals("[5, 7, 6, 10, 8, 9]", a.toString());
        
        a.add(4);
        assertEquals("[4, 7, 5, 10, 8, 9, 6]", a.toString());
        
        a.add(4);
        assertEquals("[4, 4, 5, 7, 8, 9, 6, 10]", a.toString());
    }
  
    @Test
    public void test_B() {
        BinaryHeap<Integer> a = new BinaryHeap<Integer>();
        
        a.add(9);
        a.add(8);
        a.add(7);
        a.add(6);
        a.add(5);
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        
        assertEquals("[1, 3, 2, 4, 7, 8, 5, 9, 6]", a.toString());
        assertEquals(1, (int) a.getMin());
        assertEquals("[2, 3, 5, 4, 7, 8, 6, 9]", a.toString());
    }
    
    @Test
    public void test_C() {
        BinaryHeap<Character> a = new BinaryHeap<Character>();
        
        a.add('f');
        a.add('g');
        a.add('a');
        a.add('z');
        a.add('d');
        
        assertEquals("[a, d, f, z, g]", a.toString());
        assertEquals('a', (char) a.getMin());
        assertEquals("[d, g, f, z]", a.toString());
    }
    
}
