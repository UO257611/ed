package graphPerformance;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;

public class TestBench {
	public final static int SLEEP_TIME=2;
	
	/**
	 * the main method were I use java reflection
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	//	test("testLinear.txt","Algorithm","linear",3,1,50);
//		test("testQuadratic.txt","Algorithm","quadratic",3,1,50);
	//	test("testCubic.txt","Algorithm","cubic",3,1,20);
	//	test("testpowRec1.txt","Algorithm","powRec1",3,1,12);
//		test("testpowRec2.txt","Algorithm","powRec2",3,1,50);
//		test("testpowRec3.txt","Algorithm","powRec3",3,1,50);
  //	test("testpowRec4.txt","Algorithm","powRec4",3,1,50);
	  //test("testfactorialRec.txt","Algorithm","factorialRec",3,1,50);
		

	}
/**
 *  the principal method which execute the benchmark
 * 
 * @param outputFilename the name of the .txt file
 * @param classname the name of the class
 * @param method the name of the method 
 * @param samples the number of repetition
 * @param startN the start interval
 * @param endN the finish interval
 * 
 *
 */
	public static void test (String outputFilename,String classname, String method,int samples , int startN, int endN) {
		//Executes an algorithm for a given workload "n"
		FileWriter file = null;
		PrintWriter pw;
		
		try {
			file = new FileWriter(outputFilename);
			pw= new PrintWriter(file);
			Class myClass = Class.forName("graphPerformance."+classname);
			Object myObject = myClass.newInstance();
			Method myMethod = myClass.getDeclaredMethod(method, int.class);
			
			for (int i=startN; i<endN; i++)
			{long totalTime = 0;
				for (int j = 0; j< samples; j++) {
					
				long startTime = System.currentTimeMillis();
				myMethod.invoke(myObject, i);
				long endTime = System.currentTimeMillis();
				totalTime += endTime - startTime;
				
			}
			pw.println(totalTime/samples);
			}
			 
			
		}
		catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(file !=null) {
					file.close();
				}
			}catch(Exception e2) {
				e2.printStackTrace();
			}
		}
		/*
		Algorithm.linear(n);
		long finalTime = System.currentTimeMillis();
		System.out.println("I wasted "+ (finalTime-startTime)+" miliseconds of my life");
		*/
	}
	/**
	 * a method that do nothing
	 * 
	 * @param i the number of repetitions
	 * 
	 */
	public static void  doNothing(long i) {
		System.out.println("Doing nothing at iteration..."+i);
		long endTime = System.currentTimeMillis()+SLEEP_TIME;
		while(System.currentTimeMillis() < endTime) {
			//I ' M 	S L E E P I N G
		}
		
		
	}

	/** * 
	 * a method that do nothing but with the implementation of the algorithm
	 * 
	 * 
	 * @param i the number of repetitions
	 */
	public static void doNothingLogarithmic(double d) {
		System.out.println("Doing nothing at iteration..."+d);
		long endTime = System.currentTimeMillis()+SLEEP_TIME;
		while(System.currentTimeMillis() < endTime) {
			//I ' M 	S L E E P I N G
		}
		
	}
	
}
	