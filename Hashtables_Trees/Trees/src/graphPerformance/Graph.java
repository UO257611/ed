package graphPerformance;

import java.util.ArrayList;

public class Graph<T> {
	public static final  int INDEX_NOT_FOUND=-1;
	
	private ArrayList<GraphNode<T>> nodes;
	private boolean[][] edges;
	private double [][] weight;
	
	private double[] D; 	//The cost vector of the Dijkstra algorithm
	private int[] PD;   	//The Dijkstra's pathway vector
	
	private double[][] A; 	//The cost matrix of the Floyd algorithm
	private int[][]P;   	//The Floyd pathway matrix
	
	public final static double INFINITE =99.0;
	private final static double EMPTY =-1;
	
	private int maxSize;
	
	
	/**
	 * The constructor of the class
	 * @param n
	 */
	public Graph(int n) {
		nodes = new ArrayList<GraphNode<T>>();
		maxSize = n;
		edges= new boolean[n][n];
		weight= new double [n][n];
		D= new double[n];
		PD= new int[ n];
		P= new int[n][n];
		A = new double[n][n];
		

		
	}
	/**
	 * Method to return the arraylist of nodes
	 * @return araylist nodes
	 */
	protected ArrayList<GraphNode<T>> getNodes() {
		return nodes;
	}

	
	/**
	 * Method that add a new node to the arraylist
	 * @param node, the new node
	 */
	public void setNodes(ArrayList<GraphNode<T>> node) {
		this.nodes = node;
	}

	/**
	 * Method that return the edges matrix
	 * @return edges matrix
	 */
	public boolean[][] getEdges() {
		return edges;
	}

	
	/**
	 * Method that add new conection between to nodes 
	 * @param edges to add
	 */
	public void setEdges(boolean[][] edges) {
		this.edges = edges;
	}

	/**
	 *  Method that return the weight matrix
	 * @return weight matrix
	 */
	public double[][] getWeight() {
		return weight;
	}

	/**
	 * Method that add weight to a posisition in the weight matrix
	 * @param weight to add
	 */
	public void setWeight(double[][] weight) {
		this.weight = weight;
	}
	
	/**
	 * Method that return the size of the node arraylist
	 * @return the size of that list
	 */
	public int  getSize() {
		return nodes.size();
		}
	
	/**
	 * Method that return the position on the arraylist of a node
	 * @param element of the node to search
	 * @return int i, the position
	 */
	public int getNode(T element){
		for(int i=0; i< nodes.size(); i++) {
			GraphNode<T>node = nodes.get(i);
			if (node.getElement().equals(element)) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Method that add a new node with a determinate element
	 * @param element of the new node
	 * @throws Exception, the element is repeated
	 */
	public void addNode(T element) throws Exception {
		if(getNode(element)!=-1) {
			throw new RuntimeException("Error there is a repeated node");
		}
		for( int i=0 ; i< getSize(); i++) {
			edges[i][getSize()]=false;
			edges[getSize()][i]=false;
			
			
		}
		
			nodes.add(new GraphNode<T>(element));
		
	}
	
	
	/**
	 * Method that add an edge in a determinate position
	 * @param org, the origin node
	 * @param dest, the destination node
	 * @param weight, the weight of taking this edge
	 * @throws Exception, the org or the dest node doesnt exist
	 */
	public void addEdge(T org, T dest, double weight) throws Exception{{
	
	if( !existsEdge(org, dest)) {
		int i = getNode(org);
		int j = getNode(dest);
		
		 edges[i][j] = true;
		 this.weight[i][j]= weight;}
	
}
}
	/**
	 * Method that check if the edge estist
	 * @param orig, origin node
	 * @param dest, destination node
	 * @return boolean true if exist, false if not
	 * @throws Exception
	 */
	public boolean existsEdge(T orig, T dest) throws Exception{
		int i = getNode(orig);
		int j = getNode(dest);
		
		if(i != -1 && j!= -1) {
			return edges[i][j];}
		else {
			throw new Exception("there is an error in the existEdge method");
		}
	}

	
	
	/**
	 * Method that delete a node with a determinate element
	 * @param element, element of the node to delete
	 * @throws Exception
	 */
	public void removeNode(T element) throws Exception{
		int i=  getNode(element);
		int x = getSize();
		 if (i== -1) {
			throw new Exception("element not exist");
		 }
		 nodes.set(i, nodes.get(x-1));
		 nodes.remove(x-1);
		 
		
		 if (i != getSize()) {
			 for (int j = 0; j< getSize(); j++) {
					if(i!= x && j!=x) {
					edges[i][j]= edges[x][j];
					edges[j][i]= edges[j][x];
					weight[i][j]= weight[x][j];
					weight[j][i]= weight[j][x];
					
					
					
				}
					edges[i][i]= edges[x][x];
					weight[i][i]= weight[x][x];}
				
				
		 }
		 
	}
	
	/**
	 * Method that remove an edge of a determinate position
	 * @param orig, origin node
	 * @param dest, destination node
	 * @throws Exception
	 */
	public void removeEdge(T orig, T dest)throws Exception{
		
		if( existsEdge(orig, dest)) {
			int i = getNode(orig);
			int j = getNode(dest);
			
			 edges[i][j] = false;}
		
	}
	
	/**
	 * Method that print the information of node arraylist, the edge and weight matrix
	 */
	public void print() {
		System.out.print("Nodes elements in order: ");
		for(GraphNode<T> n : nodes)
		{
			System.out.print(n.getElement()+", ");
		}
		System.out.println("");
		System.out.print("Edges: ");
		System.out.println(" { ");
		for (int i= 0; i< edges.length;i++) 
		{
			for(int j=0; j<edges[i].length; j++)
			{
				System.out.print(edges[i][j]+ " ,");
			}
			System.out.println("");
		}
		System.out.print("}");
		
		System.out.println("");
		System.out.print("Weight: ");
		System.out.println(" { ");
		for (int i= 0; i< weight.length;i++) 
		{
			for(int j=0; j<weight[i].length; j++)
			{
				System.out.print(weight[i][j]+" ,");
			}
			System.out.println("");
		}
		System.out.print("}");
		
	}
	
	
/**
 * it reset the visited flag of all the nodes and it select and start printing the path
 * @param element, the first element of the path
 * @return the string eith the path
 */
public String traverseGraphDF(T element) {
	resetVisitedFlag();
	
	int i = getNode( element);
	
	if(i!=-1) {
	return DFPrint(i);
	}
	else
		return null;
}

private void resetVisitedFlag() {
	for(GraphNode<T> node: nodes) {
		node.setVisited(false);
	}
}
/**
 * Printing the path of the depth-first algorithm, 
 *  
 * @param the position of the node of the arrayList
 * @return the String with the path
 */
private String DFPrint(int v) {
	nodes.get(v).setVisited(true);
	String aux= nodes.get(v).getElement().toString()+"-";
	
	for(int i= 0; i< getSize(); i++) {
		if ( edges[v][i]==true && !nodes.get(i).isVisited() )
		
		aux+= DFPrint(i);
	}
	
	return aux;
}

/**
 * in this method we count the number of edges going in of the node
 * 
 * @param element in which we are going to count
 * @return the number of edges going in
 */
public int inDegree(T element) {
	int i = getNode(element);
	int counter = 0;
	if( i != -1) {
		for(int j = 0 ; j< getSize();j++) {
			if ( edges[j][i]) {
				counter++;
			}
		}
	}
	return counter;
}
/**
 * in this method we count the number of edges going out of the node
 * 
 * @param element in which we are going to count
 * @return the number of edges going out
 */
public int outDegree(T element) {
	int i = getNode(element);
	int counter = 0; 
	
	if(i != -1) {
		for( int j = 0; j < getSize(); j++) {
			if (edges[i][j]) {
				counter++;
			}
		}
	}
	return counter++;
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//Here start the DIJKSTRA methods/////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

/**
 * Method that return the weight matrix of the dijkstra algorithm
 * @return the D matrix
 */
public double[][] getD(){
	double[][] myArray = new double[1][D.length];
	for(int i=0; i<D.length; i++) {
		myArray[0][i]= D[i];
	}
	return myArray;
}

/**
 * Method that return the dijkstra pathway vector
 * @return vector PD
 */
public int[] getPD() {
	return  PD;
}

/**
 * Method that initialize the PD vector, the D matrix, the edge and weight matrix, for after using the Dijkstra algorithm
 * @param departureNode, the node from it will start the dijkstra algorithm
 */
private void initDijkstra(T departureNode) {
	
	
	int node =  getNode(departureNode);
	for(int i=0; i<D.length;i++) {
		PD[node]= -1;
		if(edges[node][i]) {
			D[i] = weight[node][i] ;
			PD[i] = node;
		}
		else {
			D[i]= INFINITE;
			
		}
	}
	resetVisitedFlag();
	//nodes.get(node).setVisited(true);
	
}


/**
 * Method that implements the Dijkstra algorithm
 * @param departureNode, the node from it will start
 */
public void Dijkstra(T departureNode) {
	initDijkstra(departureNode);
	
	for(int p= 0; p< getSize(); p++) {
		
		double minValue=INFINITE;
		int w= -1;
		
		for(int i = 0;i <getSize(); i++) {
			if ( !nodes.get(i).isVisited() && D[i]<minValue) {
				minValue= D[i];
				w= i;
			}
		}
		
		if(w>=0) {
			nodes.get(w).setVisited(true);
			
			for( int m= 0; m< getSize(); m++) {
				if( edges[w][m]) {
					if(D[w]+weight[w][m] <D[m]) {
						D[m]= weight[w][m]+D[w];
						PD[m]= w;
					}
					
				}
			
				
			}
		}
		else {
			System.out.println("Pivot not found");
			for(int x=0; x< PD.length;x++) {
				if(!nodes.get(x).isVisited())
				{
					PD[x]=-1;
				}
			}
		}
	}

	
	
		
		
	}

/**
 * This method will print the dijkstra pathway (PD vector)
 * @param departure node
 * @param arrival node
 * @return string with the pathway
 * @throws Exception
 */
public String printDijkstraPath(T departure, T arrival) throws Exception
{
	
	int i = getNode(departure);
	int j = getNode(arrival);
	
	

	if(j == INDEX_NOT_FOUND ||i == INDEX_NOT_FOUND)
	{
		throw new Exception();
	}
	int k =PD[j];
	int r= PD[i];
	
	if (D[j]<99.0) 
	{
	if(k!=i)
		
	{
		
		return printDijkstraPath(departure, nodes.get(k).getElement())+ arrival;
	}
	else
	{
		return  (String) departure+ arrival;
	}
	}
	else
	{
		return "";
	}
	
	
	
	

	}




/**
 * This method print the information of the Dijkstra after the Dijkstra method
 */
public void printDijkstra() {
	System.out.println("The information of the dijkstra weight (D vector): ");
	System.out.print("{ ");
	for (int i = 0; i< D.length ; i++)
	{
		System.out.print(D[i]+ ", ");
	}
	System.out.print("} ");
	System.out.println("");
	
	System.out.println("The information of the dijkstra weight (PD vector): ");
	System.out.print("{ ");
	for (int i=0;i< PD.length;i++)
	{
		System.out.print(PD[i]+ ", ");
	}
	System.out.print("} ");
	System.out.println("");
	
}



//////////////////////////////////////////////////
//////////////////////////////////////////////////
//Here start the FLOYD methods/////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

/**
 * This method initialize the basic vector and matrixes for Floyd algorithm 
 */
private void initsFloyd()
{
	for(int i= 0; i<getSize();i++)
	{
		for(int j =0; j<getSize();j++)
		{
			if(edges[i][j])
			{
				A[i][j]= weight[i][j];
			}
			else
			{
			
				A[i][j]= INFINITE;
				
			}
			A[i][i]=0;
			P[i][j]=-1;
		}
	}
	
}

/**
 * Method that return the weight matrix of the Floyd algorithm
 * @return A matrix
 */
public double[][] getA() {
	return A;
}


/**
 * This method return the Floyd pathway matrix
 * @return P matrix
 */
public int[][] getP() {
	return P;
}


/**
 * Method that implements the Floyd algorithm
 * @param An size of the node matrix
 */
public void floyd(int An)
{
	initsFloyd();
	if (An<=getSize()) 
	{
		
		
		for(int k=0; k<An; k++ ) 
		{
			
			for(int i=0; i<getSize(); i++) 
			{
			
				for(int j=0; j<getSize(); j++)
				{
					if(A[i][k]+A[k][j] < A[i][j])
					{
						A[i][j]= A[i][k]+A[k][j];
						P[i][j]= k ;
					}
					
				}
			}
			
			
			
			
		}
		
		
		
	}
}



/**
 * This method print the floyd pathway
 * @param departure node
 * @param arrival node
 * @return String with the pathway
 * @throws Exception
 */
public String printFloydPath(T departure, T arrival) throws Exception
{
	int i = getNode(departure);
	int j = getNode(arrival);
	


	if(j == INDEX_NOT_FOUND ||i == INDEX_NOT_FOUND)
	{
		throw new Exception();
	}
	
	int k = P[i][j];
	if(A[i][j]<99) {
	if(k!= i && k>=0 )
	{
		

		return printFloydPath(departure, nodes.get(k).getElement()  )+nodes.get(k).getElement()+printFloydPath(nodes.get(k).getElement(), arrival);
}
	
	else
	{
		return "";
	}
	}
	return "" ;
	}


	
	
	
	
	
}



	







