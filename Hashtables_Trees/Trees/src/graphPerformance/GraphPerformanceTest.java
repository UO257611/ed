package graphPerformance;
import java.util.Random;

public class GraphPerformanceTest 
{
	public static void main(String[] args) 
	{
		
		TestBench.test("01_Graph_Build.txt","GraphPerformanceTest", "initGraph",3, 100, 300);
		TestBench.test("02_Graph_Dijkstra.txt","GraphPerformanceTest", "runDijkstra", 3, 100, 300);
		TestBench.test("03_Graph_Floyd.txt","GraphPerformanceTest","runFloyd", 3, 100, 300);

	
	}

	public static Graph<Integer> initGraph(int n) throws Exception
	{ 	Random random = new Random();
		Graph<Integer> x = new Graph(n);
		for (int i = 0; i< n; i++)
		{

			x.addNode(i);
		
			
		}
		for (int i = 0; i< n; i++)
		{
			for( int j = 0; j< n; j++)
			{
				x.addEdge(i, j, random.nextInt(50)+1);
				
				
			}
			
		}
		return x;
		
	}
	
	
	public static void runDijkstra(int n) throws Exception 
	{
		initGraph(n).Dijkstra(0);
		
		
	}
	public static void runFloyd(int n) throws Exception 
	{
		initGraph(n).floyd(0);
		
		
	}
	
}
