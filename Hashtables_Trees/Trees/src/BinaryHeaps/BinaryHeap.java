package BinaryHeaps;

import java.util.ArrayList;
import java.util.Collections;

public class BinaryHeap<T  extends Comparable<T>>  
{
	private ArrayList<T> heap;

	public BinaryHeap() 
	{
		heap = new ArrayList<T>() ;
	}
	
/*	
	public BinaryHeap(T[] elements)
	{
		heap = new ArrayList<T>();
		if (elements.length != 0)
		{
			int counter = 0;
			for(T element: elements)
			{
				heap.add(element);

			}
			int x = heap.size()-1;
			T element;
			boolean change = false;
			
			do
			{
				x = heap.size()-1;
				change= false;
				
				while(x>0)
				{
					element = heap.get(x);
					filterUp(x);
					if(element.compareTo(heap.get(x))!= 0)
					{
						change= true;
					}
					x--;
				}
			}while(change);
			
		}
	}
*/

	public BinaryHeap(T[] elements)
	{
		heap = new ArrayList<T>();
			int counter = 0;
			for(T element: elements)
			{
				heap.add(element);

			}
			
			for(int i = (heap.size()-1)/2; i >= 0; i--)
			{
					filterDown(i);
			
			}
			
	}

	

	public boolean isEmpty()
	{
		return heap.isEmpty();
	}
	public String toString()
	{
		String str= "[";
		
		for (int i=0; i< heap.size();i++)
		{
			str+=heap.get(i);
			
			if(i < (heap.size()-1))
			{
				str += ", ";
			}
		}
		str+="]";
		return str;
		
	}
	
	public void print()
	{
		
	}
	
	public void filterUp(int pos)
	{
		
		while(pos>0)
		{
			int parent = (pos-1)/2;
			
			if( heap.get(pos).compareTo(heap.get(parent)) > 0)
			{
				break;
			}
			
			Collections.swap(heap,pos,parent);
			pos= parent;
		}
	}
	
	public void add(T element) 
	{
		try
		{
		if(element == null)
		{
			throw new Exception("You have introcuce a null element");
		}
		heap.add(element);
		filterUp(heap.size()-1);
		}
		catch(Exception e)
		{
			;
		}
	}
	
	
	private void filterDown(int pos)
	{
		while(pos<(heap.size())/2)
		{
			
			int child = 2*pos+1;

			if(child < heap.size()-1) {
				if(heap.get(child+1).compareTo(heap.get(child)) < 0)
				{
					child++;
				}
			}
			
			if(heap.get(pos).compareTo(heap.get(child)) < 0)
			{
				break;
			}
			
			Collections.swap(heap, pos, child);
			pos = child;
		}
	}
	
	
	public T getMin()
	{
		T removedNode = heap.get(0);
		T lastNode = heap.remove(heap.size()-1);
		
		if(heap.size()>0 )
		{
			heap.set(0, lastNode);
			filterDown(0);
				
		}
		
		
		return removedNode;
	}
	
	
	


}
