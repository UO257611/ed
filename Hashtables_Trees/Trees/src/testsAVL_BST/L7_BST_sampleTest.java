package testsAVL_BST;
import static org.junit.Assert.*;
import org.junit.Test;

import AVL.AVLTree;

public class L7_BST_sampleTest {
    
    @Test
    public void test_A() {
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            a.add('a');
            a.add('d');
            a.add('c');
            a.add('g');
            a.add('i');
            a.add('h');
        } catch (Exception ex) {
            fail();
        }
        
       // assertEquals("ba--dc--g-ih---", a.toString()); // BST tree
        assertEquals("b(4)a(0)--d(3)c(0)--g(2)-i(1)h(0)---", a.toString()); // AVL tree
        
        try {
            a.remove('b');
        } catch (Exception ex) {
            fail();
        }
        //  assertEquals("a-dc--g-ih---", a.toString()); // BST tree
        assertEquals("a(4)-d(3)c(0)--g(2)-i(1)h(0)---", a.toString()); // AVL tree
        
        try {
            a.remove('g');
        } catch (Exception ex) {
            fail();
        }
        //  assertEquals("a-dc--ih---", a.toString()); // BST tree
        assertEquals("a(3)-d(2)c(0)--i(1)h(0)---", a.toString()); // AVL tree
        
        try {
            a.remove('k');
            fail();
        } catch (Exception ex) {
            System.out.println("The element does not exist");
        }
        
    }
    
    @Test
    public void test_B() throws Exception {
        
        AVLTree<Character> a = new AVLTree<Character>();
        try {
            a.add('b');
            a.add('a');
            a.add('d');
        } catch (Exception ex) {
            fail();
        }
        
        AVLTree<Character> b = new AVLTree<Character>();
        try {
            b.add('c');
            b.add('g');
            b.add('i');
            b.add('d');
        } catch (Exception ex) {
            fail();
        }
        
        assertEquals("b(3)a(0)--d(2)c(0)--g(1)-i(0)--", a.joins(b).toString());
        
    }
    
}
