package testsAVL_BST;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import AVL.AVLTree;

public class testAVLTree {
	AVLTree<Integer> a;
	AVLTree<Integer> c,b,d;
	AVLTree<Character> z;
	@Before
	public void setUp() throws Exception 
	{
		z= new AVLTree<Character>();
		a= new AVLTree<Integer>();
		c= new AVLTree<Integer>();
		d= new AVLTree<Integer>();
		
	}
	
	@Test
	public void testGetHeight()
	{
		try {
			z.add('b'); 
			z.add('a'); 
			z.add('d'); 
			z.add('c'); 
			z.add('g'); 
			z.add('i'); 
			z.add('h');
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals(3, z.getHeight());
		
		
		try
		{
			a.add(4);
			a.add(2);
			a.add(1);
			a.add(3);
			a.add(8);
			a.add(6);
			a.add(10);
			a.add(5);
			a.add(7);
			a.add(11);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals(4, a.getHeight());
		assertEquals(0,c.getHeight());
	}
	
	@Test
	public void testSimpleRotation()
	{
		try
		{
			a.add(7);
			a.add(6);
			a.add(5);
			a.add(4);
			a.add(3);
			a.add(1);
		}
		catch(Exception e)
		{
			fail();
		}
		
		assertEquals("4(0)3(-1)1(0)---6(0)5(0)--7(0)--", a.toString());
		
		try
		{
			a.add(8);
			a.add(9);
		}
		catch(Exception e)
		{
			fail();
		}

		assertEquals("4(1)3(-1)1(0)---6(1)5(0)--8(0)7(0)--9(0)--", a.toString());
		
		try 
		{
			a.add(10);
		}
		catch(Exception e)
		{
			fail();
		
		}
		assertEquals("4(1)3(-1)1(0)---8(0)6(0)5(0)--7(0)--9(1)-10(0)--"
				+ "", a.toString());
	}
	
	
	
	
	@Test
	public void testDoubleRotation()
	{
		try
		{
			a.add(1);
			a.add(2);
			a.add(3);
			a.add(4);
			a.add(5);
			a.add(6);
			a.add(10);
			a.add(11);
			a.add(8);
			a.add(7);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals("4(1)2(0)1(0)--3(0)--8(0)6(0)5(0)--7(0)--10(1)-11(0)--", a.toString());
		//
		try 
		{
			a.add(15);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals("4(1)2(0)1(0)--3(0)--8(0)6(0)5(0)--7(0)--11(0)10(0)--15(0)--", a.toString());
		
		try
		{
			a.add(20);
			a.add(12);
			a.add(17);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals("8(0)4(0)2(0)1(0)--3(0)--6(0)5(0)--7(0)--15(0)11(0)10(0)--12(0)--20(-1)17(0)---", a.toString());
		
		
	}
	
	@Test
	public void testSeminarios()
	{
		try {
		d.add(26);
		d.add(17);
		d.add(5);
		d.add(21);
		d.add(24);
		d.add(3);
		d.add(8);
		d.add(6);
		d.add(10);
		d.add(15);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println(d.toString());
		try {
			d.remove(24);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(d.toString());
		
		try {
			d.remove(26);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(d.toString());
		
		try {
			d.remove(21);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(d.toString());
		
	
	
	}
	
	
}
