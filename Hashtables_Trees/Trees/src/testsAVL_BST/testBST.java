package testsAVL_BST;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import AVL.AVLNode;
import AVL.AVLTree;

public class testBST {
	AVLTree<Integer> a;
	AVLTree<Integer> c,b;
	AVLTree<Character> z;
	@Before
	public void setUp() throws Exception 
	{
		a= new AVLTree<Integer>();
		b= new AVLTree<Integer>();
		c= new AVLTree<Integer>();
		z= new AVLTree<Character>();
		
		
	}

	@Test
	public void testAdd() throws Exception {
		try {
		a.setRoot(new AVLNode<Integer>(6));
		a.add(4);
		a.add(3);
		a.add(5);
		a.add(12);
		a.add(9);
		a.add(8);
		a.add(11);
		
	
		assertEquals("6(3)4(1)3(0)--5(0)--12(2)9(1)8(0)--11(0)---", a.toString());
		
		a.add(2);
		
		assertEquals("6(3)4(2)3(1)2(0)---5(0)--12(2)9(1)8(0)--11(0)---", a.toString());
		}
		catch(Exception e)
		{
			fail();
		}
		
		try 
		{
			a.add(11);
		}
		catch(Exception e)
		{
			
		}
		
	}
	
	@Test
	public void testRemove() {
		a.setRoot(new AVLNode<Integer>(6));
		try
		{
			a.add(4);
			a.add(3);
			a.add(5);
			a.add(12);
			a.add(9);
			a.add(8);
			a.add(11);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals("6(3)4(1)3(0)--5(0)--12(2)9(1)8(0)--11(0)---", a.toString());
		
		try 
		{
			a.remove(12);
		} 
		catch (Exception e1) 
		{
			fail();
		}
		
		assertEquals("6(2)4(1)3(0)--5(0)--9(1)8(0)--11(0)--", a.toString());
		
		try 
		{
			a.remove(6);
		} 
		catch (Exception e1) 
		{
			fail();
		}
		
		assertEquals("5(2)4(1)3(0)---9(1)8(0)--11(0)--", a.toString());
		
		
		try 
		{
			a.remove(30);
		}
		catch(Exception e)
		{
			
		}
		
		
	}
	
	
	@Test
	public void testJoin() {
		try
		{
		a.setRoot(new AVLNode<Integer>(6));
		a.add(4);
		a.add(3);
		a.add(5);
		a.add(12);
		a.add(9);
		a.add(8);
		a.add(11);
		
		b.add(12);
		b.add(9);
		b.add(8);
		b.add(7);
		
		c.add(1);
		c.add(20);
		c.add(11);
		}
		catch(Exception e)
		{
			fail();
		}
	
	
		assertEquals("6(3)4(1)3(0)--5(0)--12(2)9(1)8(0)--11(0)---", a.toString());
		
		
		
		assertEquals("6(4)4(1)3(0)--5(0)--12(3)9(2)8(1)7(0)---11(0)---", a.joins(b).toString());
		
		
		assertEquals("6(3)4(2)3(1)1(0)---5(0)--12(2)9(1)8(0)--11(0)--20(0)--", a.joins(c).toString());
		
		
		
	}
	
	
	@Test
	public void testGetMax() 
	{
	
		a.setRoot(new AVLNode<Integer>(6));
		try
		{
			a.add(4);
			a.add(3);
			a.add(5);
			a.add(12);
			a.add(9);
			a.add(8);
			a.add(11);
		}
		catch(Exception e)
		{
			fail();
		}
		
		assertEquals(12, (int)a.getMax(a.getRoot()));
		
		try {
			a.remove(12);
		} catch (Exception e) {
			fail();
		};
		
		assertEquals(11, (int)a.getMax(a.getRoot()));
		
		
	}
	
	

	
	
	

}
