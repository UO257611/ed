package testsAVL_BST;

import static org.junit.Assert.*;

import org.junit.Test;

import AVL.AVLNode;
import AVL.AVLTree;

public class Martins_Tests {
    
    @Test
    public void test1_AVLNode() {
        
        AVLNode<String> a = new AVLNode<String>("arbol");
        
        assertEquals(a.getElement(), "arbol");
        assertEquals(a.getLeft(), null);
        assertEquals(a.getRight(), null);
        
        AVLNode<Integer> b = new AVLNode<Integer>(10);
        AVLNode<Integer> c = new AVLNode<Integer>(10);
        
        assertEquals(0, b.getElement().compareTo(c.getElement()));
        c.setElement(15);
        assertEquals(-1, b.getElement().compareTo(c.getElement()));
        c.setElement(5);
        assertEquals(1, b.getElement().compareTo(c.getElement()));
        
        AVLNode<Character> aChar = new AVLNode<Character>('a');
        AVLNode<Character> bChar = new AVLNode<Character>('b');
        
        assertEquals(-1, aChar.getElement().compareTo(bChar.getElement()));
        
    }
    
    @Test
    public void test2() {
        
        AVLTree<Character> charTree = new AVLTree<Character>();
        
        try {
            charTree.add('a');
            charTree.add('b');
            charTree.add('c');
        } catch (Exception ex) {
            fail();
        }
        
        try {
            charTree.add('b');
            fail();
        } catch (Exception ex) {
            System.out.println("No repeated nodes are allowed: " + ex);
        }
        
        assertEquals("a-b-c--", charTree.toString());
        
        try {
            charTree.add('d');
        } catch (Exception ex) {
            fail();
        }
        
        assertEquals("a-b-c-d--", charTree.toString());
        
    }
    
    @Test
    public void test3() {
        
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            a.add('a');
            a.add('d');
            a.add('c');
            a.add('g');
            a.add('i');
            a.add('h');
        } catch (Exception ex) {
            fail();
        }
        
        assertEquals("ba--dc--g-ih---", a.toString());
        
        assertEquals(true, a.search('i'));
        assertEquals(false, a.search('f'));
        
        assertEquals(0, a.getMax(a.getRoot()).compareTo('i'));
        assertEquals(0, a.getMax(a.getRoot().getRight()).compareTo('i'));
        assertEquals(0, a.getMax(a.getRoot().getLeft()).compareTo('a'));
        
    }
    
}
