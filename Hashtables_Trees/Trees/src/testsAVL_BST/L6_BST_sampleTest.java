package testsAVL_BST;

import static org.junit.Assert.*;
import org.junit.Test;

import AVL.AVLTree;

public class L6_BST_sampleTest {
    
    @Test
    public void test_A() {
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            assertEquals("b--", a.toString());
        } catch (Exception ex) {
            fail();
        }
        
        try {
            a.add('a');
            assertEquals("ba---", a.toString());
        } catch (Exception ex) {
            fail();
        }
        
        try {
            a.add('d');
            assertEquals("ba--d--", a.toString());
        } catch (Exception ex) {
            fail();
        }
        
        try {
            a.add('c');
            assertEquals("ba--dc---", a.toString());
        } catch (Exception ex) {
            fail();
        }
        
        try {
            a.add('g');
            assertEquals("ba--dc--g--", a.toString());
        } catch (Exception ex) {
            fail();
        }
        
        try {
            a.add('b');
            fail();
        } catch (Exception ex) {
            System.out.println("No repeated nodes are allowed: " + ex);
        }
        
    }
    
    @Test
    public void test_B() {
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            a.add('a');
            a.add('d');
            a.add('c');
            a.add('g');
            a.add('i');
            a.add('h');
        } catch (Exception ex) {
            fail();
        }
        
        assertEquals(true, a.search('i'));
        assertEquals(false, a.search('f'));
        
        assertEquals('i', (char) a.getMax(a.getRoot()));
    }
    
}


