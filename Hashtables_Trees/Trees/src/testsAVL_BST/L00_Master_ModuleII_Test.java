package testsAVL_BST;

import org.junit.Test;

import AVL.AVLNode;
import AVL.AVLTree;
import BinaryHeaps.BinaryHeap;

import static org.junit.Assert.*;

public class L00_Master_ModuleII_Test {
    
    // AVL trees: test1 - test6
    // Binary heaps: test7 - test8
    // Hash tables: test9 - test13
    
    @Test
    public void test1() {
        
        AVLNode<String> a = new AVLNode<String>("arbol");
        
        assertEquals(a.getElement(), "arbol");
        assertEquals(a.getLeft(), null);
        assertEquals(a.getRight(), null);
        assertEquals(a.getHeight(), 0);
        
        AVLNode<Integer> b = new AVLNode<Integer>(10);
        AVLNode<Integer> c = new AVLNode<Integer>(10);
        
        assertEquals(0, b.getElement().compareTo(c.getElement()));
        c.setElement(15);
        assertEquals(-1, b.getElement().compareTo(c.getElement()));
        c.setElement(5);
        assertEquals(1, b.getElement().compareTo(c.getElement()));
        
        AVLNode<Character> aChar = new AVLNode<Character>('a');
        AVLNode<Character> bChar = new AVLNode<Character>('b');
        
        assertEquals(-1, aChar.getElement().compareTo(bChar.getElement()));
        
    }
    
    @Test
    public void test2() {
        
        AVLTree<Character> charTree = new AVLTree<Character>();
        
        try {
            charTree.add('a');
            charTree.add('b');
            charTree.add('c');
        } catch (Exception ex) {
            fail();
        }
        
        try {
            charTree.add('b');
            fail();
        } catch (Exception ex) {
            System.out.println("No repeated nodes are allowed: " + ex);
        }
        assertEquals("b(0)a(0)--c(0)--", charTree.toString());
        
        try {
            charTree.add('d');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("b(1)a(0)--c(1)-d(0)--", charTree.toString());
        
    }
    
    @Test
    public void test3() {
        
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            a.add('a');
            a.add('d');
            a.add('c');
            a.add('g');
            a.add('i');
            a.add('h');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("d(0)b(0)a(0)--c(0)--h(0)g(0)--i(0)--", a.toString());
        
        assertEquals(true, a.search('i'));
        assertEquals(false, a.search('f'));
        
        assertEquals(0, a.getMax(a.getRoot()).compareTo('i'));
        assertEquals(0, a.getMax(a.getRoot().getRight()).compareTo('i'));
        assertEquals(0, a.getMax(a.getRoot().getLeft()).compareTo('c'));
        
    }
    
    @Test
    public void test4() {
        
        AVLTree<Integer> ti = new AVLTree<Integer>();
        
        try {
            ti.add(10);
            ti.add(6);
            ti.add(15);
            ti.add(3);
            ti.add(9);
            ti.add(14);
            ti.add(20);
            ti.add(2);
            ti.add(4);
            ti.add(7);
            ti.add(12);
            ti.add(1);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(-1)6(-1)3(-1)2(-1)1(0)---4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(25);
            fail();
        } catch (Exception ex) {
            System.out.println("The element does not exist: " + ex);
        }
        
        try {
            ti.remove(3);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(0)6(0)2(0)1(0)--4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(1);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(0)6(0)2(1)-4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(2);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(0)6(1)4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(9);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(1)6(0)4(0)--7(0)--15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(10);
            ti.remove(4);
            ti.remove(7);
            ti.remove(15);
            ti.remove(20);
            ti.remove(12);
            ti.remove(14);
            ti.remove(6);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("-", ti.toString());
        
    }
    
    @Test
    public void test5() {
        
        AVLTree<Character> a = new AVLTree<Character>();
        AVLTree<Character> b = new AVLTree<Character>();
        
        try {
            a.add('a');
            a.add('b');
            a.add('c');
        } catch (Exception ex) {
            fail();
        }
        
        try {
            b.add('c');
            b.add('d');
            b.add('e');
        } catch (Exception ex) {
            fail();
        }
        
        assertEquals("b(1)a(0)--d(0)c(0)--e(0)--", a.joins(b).toString());
        assertEquals("d(-1)b(0)a(0)--c(0)--e(0)--", b.joins(a).toString());
        assertEquals("b(0)a(0)--c(0)--", a.toString());
        assertEquals("d(0)c(0)--e(0)--", b.toString());
        
    }
    
    @Test
    public void test6() {
        
        AVLTree<Integer> ti = new AVLTree<Integer>();
        assertEquals(0, ti.getHeight());
        
        try {
            ti.add(10);
            ti.add(6);
            ti.add(15);
            ti.add(3);
            ti.add(9);
            ti.add(14);
            ti.add(20);
            ti.add(2);
            ti.add(4);
            ti.add(7);
            ti.add(12);
            ti.add(1);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(-1)6(-1)3(-1)2(-1)1(0)---4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        assertEquals(5, ti.getHeight());
        
        try {
            ti.remove(20);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(0)3(-1)2(-1)1(0)---4(0)--10(0)9(-1)7(0)---14(0)12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(4);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(1)2(0)1(0)--3(0)--10(0)9(-1)7(0)---14(0)12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(10);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(1)2(0)1(0)--3(0)--9(1)7(0)--14(0)12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(9);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(1)2(0)1(0)--3(0)--14(-1)7(1)-12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(6);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("3(1)2(-1)1(0)---14(-1)7(1)-12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(3);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("7(0)2(-1)1(0)---14(0)12(0)--15(0)--", ti.toString());
        assertEquals(3, ti.getHeight());
    }
    
    @Test
    public void test7() {
        
        Integer[] input = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        BinaryHeap<Integer> heap = new BinaryHeap<Integer>(input);
        
        assertEquals("[1, 2, 4, 3, 6, 5, 8, 10, 7, 9]", heap.toString());
        
        // Get the elements in sorted order
        String result = "";
        while (!heap.isEmpty()) {
            int x = heap.getMin();
            result += x;
        }
        
        assertEquals("12345678910", result);
    }
    
    @Test
    public void test8() {
        
        BinaryHeap<Integer> heap = new BinaryHeap<Integer>();
        
        heap.add(2);
        heap.add(5);
        heap.add(1);
        
        assertEquals("[1, 5, 2]", heap.toString());
        
        // Get the elements in sorted order
        String result = "";
        while (!heap.isEmpty()) {
            int x = heap.getMin();
            result += x;
        }
        
        assertEquals("125", result);
    }
    /*
    @Test
    public void test9() {
        HashTable<Integer> h = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 0.5);
        assertEquals("[0] (0) = null - [1] (0) = null - [2] (0) = null - [3] (0) = null - [4] (0) = null - ", h.toString());
        
        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 0.5);
        assertEquals(2, a.f(7, 0));
        assertEquals(3, a.f(7, 1));
        assertEquals(4, a.f(7, 2));
        assertEquals(0, a.f(7, 3));
        
        HashTable<Integer> b = new HashTable<Integer>(5, HashTable.QUADRATIC_PROBING, 0.5);
        assertEquals(2, b.f(7, 0));
        assertEquals(3, b.f(7, 1));
        assertEquals(1, b.f(7, 2));
        assertEquals(1, b.f(7, 3));
        
        //        HashTable<Integer> c = new HashTable<Integer>(5, HashTable.DOUBLE_HASHING, 0.5);
        //        assertEquals(2, c.f(7, 0));
        //        assertEquals(4, c.f(7, 1));
        //        assertEquals(1, c.f(7, 2));
        //        assertEquals(3, c.f(7, 3));
    }
    
    @Test
    public void test10() {
        HashTable<Character> a = new HashTable<Character>(5, HashTable.LINEAR_PROBING, 0.5);
        assertEquals(0, a.f('A', 0));
        assertEquals(1, a.f('A', 1));
        assertEquals(2, a.f('A', 2));
        assertEquals(3, a.f('A', 3));
        
        HashTable<Character> b = new HashTable<Character>(5, HashTable.QUADRATIC_PROBING, 0.5);
        assertEquals(0, b.f('A', 0));
        assertEquals(1, b.f('A', 1));
        assertEquals(4, b.f('A', 2));
        assertEquals(4, b.f('A', 3));
        
        //        HashTable<Character> c = new HashTable<Character>(5, HashTable.DOUBLE_HASHING, 0.5);
        //        assertEquals(0, c.f('A', 0));
        //        assertEquals(1, c.f('A', 1));
        //        assertEquals(2, c.f('A', 2));
        //        assertEquals(3, c.f('A', 3));
    }
    
    @Test
    public void test11() {
        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
        
        assertEquals(false, a.isPrime(1));
        assertEquals(true, a.isPrime(2));
        assertEquals(true, a.isPrime(3));
        assertEquals(false, a.isPrime(4));
        assertEquals(true, a.isPrime(5));
        assertEquals(false, a.isPrime(6));
        assertEquals(true, a.isPrime(7));
        assertEquals(false, a.isPrime(8));
        assertEquals(false, a.isPrime(9));
        assertEquals(false, a.isPrime(10));
        assertEquals(true, a.isPrime(11));
        assertEquals(false, a.isPrime(12));
        assertEquals(true, a.isPrime(13));
        assertEquals(false, a.isPrime(14));
        assertEquals(false, a.isPrime(15));
        assertEquals(false, a.isPrime(16));
        assertEquals(true, a.isPrime(17));
        
        assertEquals(2, a.getNextPrimeNumber(1));
        assertEquals(3, a.getNextPrimeNumber(2));
        assertEquals(5, a.getNextPrimeNumber(3));
        assertEquals(5, a.getNextPrimeNumber(4));
        assertEquals(7, a.getNextPrimeNumber(5));
        assertEquals(7, a.getNextPrimeNumber(6));
        assertEquals(11, a.getNextPrimeNumber(7));
        assertEquals(11, a.getNextPrimeNumber(8));
        assertEquals(11, a.getNextPrimeNumber(9));
        assertEquals(11, a.getNextPrimeNumber(10));
        assertEquals(13, a.getNextPrimeNumber(11));
        
        assertEquals(13, a.getPrevPrimeNumber(15));
        assertEquals(13, a.getPrevPrimeNumber(14));
        assertEquals(11, a.getPrevPrimeNumber(13));
        assertEquals(11, a.getPrevPrimeNumber(12));
        assertEquals(7, a.getPrevPrimeNumber(11));
        assertEquals(7, a.getPrevPrimeNumber(10));
        assertEquals(7, a.getPrevPrimeNumber(9));
        assertEquals(7, a.getPrevPrimeNumber(8));
        assertEquals(5, a.getPrevPrimeNumber(7));
        assertEquals(5, a.getPrevPrimeNumber(6));
        assertEquals(3, a.getPrevPrimeNumber(5));
        assertEquals(3, a.getPrevPrimeNumber(4));
        assertEquals(2, a.getPrevPrimeNumber(3));
    }
  */  
    //    @Test
    //    public void test12() {
    //        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 0.5);
    //        a.add(4);
    //        assertEquals(0.2, a.getLF(), 0.1);
    //        a.add(13);
    //        assertEquals(0.4, a.getLF(), 0.1);
    //        assertEquals("[0] (0) = null - [1] (0) = null - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
    //
    //        a.add(24);
    //        assertEquals(0.27, a.getLF(), 0.1);
    //        assertEquals("[0] (0) = null - [1] (0) = null - [2] (1) = 24 - [3] (1) = 13 - [4] (1) = 4 - [5] (0) = null - [6] (0) = null - [7] (0) = null - [8] (0) = null - [9] (0) = null - [10] (0) = null - ", a.toString());
    //
    //        a.add(3);
    //
    //        assertEquals("[0] (0) = null - [1] (0) = null - [2] (1) = 24 - [3] (1) = 13 - [4] (1) = 4 - [5] (1) = 3 - [6] (0) = null - [7] (0) = null - [8] (0) = null - [9] (0) = null - [10] (0) = null - ", a.toString());
    //    }
    
    //    @Test
    //    public void test13() {
    //        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
    //        a.add(4);
    //        assertEquals(0.2, a.getLF(), 0.1);
    //        a.add(13);
    //        assertEquals(0.4, a.getLF(), 0.1);
    //        a.add(24);
    //        assertEquals(0.6, a.getLF(), 0.1);
    //        a.add(3);
    //        assertEquals(0.8, a.getLF(), 0.1);
    //
    //        assertEquals("[0] (1) = 24 - [1] (1) = 3 - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
    //        assertEquals(true, a.search(3));
    //        assertEquals(false, a.search(12));
    //
    //        a.remove(24);
    //        assertEquals("[0] (2) = 24 - [1] (1) = 3 - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
    //
    //        assertEquals(true, a.search(3));
    //        a.add(15);
    //        assertEquals(true, a.search(3));
    //        assertEquals("[0] (1) = 15 - [1] (1) = 3 - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
    //
    //        HashTable<Integer> b = new HashTable<Integer>(5, HashTable.QUADRATIC_PROBING, 1.0);
    //        b.add(4);
    //        b.add(13);
    //        b.add(24);
    //        b.add(3);
    //
    //        assertEquals("[0] (1) = 24 - [1] (0) = null - [2] (1) = 3 - [3] (1) = 13 - [4] (1) = 4 - ", b.toString());
    //        assertEquals(true, b.search(3));
    //        assertEquals(false, b.search(12));
    //
    //        b.remove(24);
    //        assertEquals("[0] (2) = 24 - [1] (0) = null - [2] (1) = 3 - [3] (1) = 13 - [4] (1) = 4 - ", b.toString());
    //
    //        assertEquals(true, b.search(3));
    //        b.add(15);
    //        assertEquals(true, b.search(3));
    //        assertEquals("[0] (1) = 15 - [1] (0) = null - [2] (1) = 3 - [3] (1) = 13 - [4] (1) = 4 - ", b.toString());
    //
    //        HashTable<Integer> c = new HashTable<Integer>(5, HashTable.DOUBLE_HASHING, 1.0);
    //        c.add(4);
    //        c.add(13);
    //        c.add(24);
    //        c.add(3);
    //
    //        assertEquals("[0] (0) = null - [1] (1) = 3 - [2] (1) = 24 - [3] (1) = 13 - [4] (1) = 4 - ", c.toString());
    //        assertEquals(true, c.search(3));
    //        assertEquals(false, c.search(12));
    //
    //        c.remove(24);
    //        assertEquals("[0] (0) = null - [1] (1) = 3 - [2] (2) = 24 - [3] (1) = 13 - [4] (1) = 4 - ", c.toString());
    //
    //        assertEquals(true, c.search(3));
    //        c.add(15);
    //        assertEquals(true, c.search(3));
    //        assertEquals("[0] (1) = 15 - [1] (1) = 3 - [2] (2) = 24 - [3] (1) = 13 - [4] (1) = 4 - ", c.toString());
    //
    //    }
    
}
