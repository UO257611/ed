package testsAVL_BST;
import org.junit.Test;

import AVL.AVLNode;
import AVL.AVLTree;

import static org.junit.Assert.*;

public class L9_AVL_sampleTest {
    
    @Test
    public void test1() {
        
        AVLNode<String> a = new AVLNode<String>("arbol");
        
        assertEquals(a.getElement(), "arbol");
        assertEquals(a.getLeft(), null);
        assertEquals(a.getRight(), null);
        assertEquals(a.getHeight(), 0);
        
        AVLNode<Integer> b = new AVLNode<Integer>(10);
        AVLNode<Integer> c = new AVLNode<Integer>(10);
        
        assertEquals(0, b.getElement().compareTo(c.getElement()));
        c.setElement(15);
        assertEquals(-1, b.getElement().compareTo(c.getElement()));
        c.setElement(5);
        assertEquals(1, b.getElement().compareTo(c.getElement()));
        
        AVLNode<Character> aChar = new AVLNode<Character>('a');
        AVLNode<Character> bChar = new AVLNode<Character>('b');
        
        assertEquals(-1, aChar.getElement().compareTo(bChar.getElement()));
        
    }
    
    @Test
    public void test2() {
        
        AVLTree<Character> charTree = new AVLTree<Character>();
        
        try {
            charTree.add('a');
            charTree.add('b');
            charTree.add('c');
        } catch (Exception ex) {
            fail();
        }
        
        try {
            charTree.add('b');
            fail();
        } catch (Exception ex) {
            System.out.println("No repeated nodes are allowed: " + ex);
        }
        assertEquals("b(0)a(0)--c(0)--", charTree.toString());
        
        try {
            charTree.add('d');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("b(1)a(0)--c(1)-d(0)--", charTree.toString());
        
    }
    
    @Test
    public void test3() {
        
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            a.add('a');
            a.add('d');
            a.add('c');
            a.add('g');
            a.add('i');
            a.add('h');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("d(0)b(0)a(0)--c(0)--h(0)g(0)--i(0)--", a.toString());
        
        assertEquals(true, a.search('i'));
        assertEquals(false, a.search('f'));
        
        assertEquals(0, a.getMax(a.getRoot()).compareTo('i'));
        assertEquals(0, a.getMax(a.getRoot().getRight()).compareTo('i'));
        assertEquals(0, a.getMax(a.getRoot().getLeft()).compareTo('c'));
        
    }
    
    @Test
    public void test4() {
        
        AVLTree<Integer> ti = new AVLTree<Integer>();
        
        try {
            ti.add(10);
            ti.add(6);
            ti.add(15);
            ti.add(3);
            ti.add(9);
            ti.add(14);
            ti.add(20);
            ti.add(2);
            ti.add(4);
            ti.add(7);
            ti.add(12);
            ti.add(1);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(-1)6(-1)3(-1)2(-1)1(0)---4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(25);
            fail();
        } catch (Exception ex) {
            System.out.println("The element does not exist: " + ex);
        }
        
        try {
            ti.remove(3);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(0)6(0)2(0)1(0)--4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(1);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(0)6(0)2(1)-4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(2);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(0)6(1)4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(9);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("10(1)6(0)4(0)--7(0)--15(-1)14(-1)12(0)---20(0)--", ti.toString());
        
        try {
            ti.remove(10);
            ti.remove(4);
            ti.remove(7);
            ti.remove(15);
            ti.remove(20);
            ti.remove(12);
            ti.remove(14);
            ti.remove(6);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("-", ti.toString());
        
    }
    
    @Test
    public void test5() {
        
        AVLTree<Character> a = new AVLTree<Character>();
        AVLTree<Character> b = new AVLTree<Character>();
        
        try {
            a.add('a');
            a.add('b');
            a.add('c');
        } catch (Exception ex) {
            fail();
        }
        
        try {
            b.add('c');
            b.add('d');
            b.add('e');
        } catch (Exception ex) {
            fail();
        }
        
        assertEquals("b(1)a(0)--d(0)c(0)--e(0)--", a.joins(b).toString());
        assertEquals("d(-1)b(0)a(0)--c(0)--e(0)--", b.joins(a).toString());
        assertEquals("b(0)a(0)--c(0)--", a.toString());
        assertEquals("d(0)c(0)--e(0)--", b.toString());
        
    }
    
    @Test
    public void test6() {
        
        AVLTree<Integer> ti = new AVLTree<Integer>();
        assertEquals(0, ti.getHeight());
        
        try {
            ti.add(10);
            ti.add(6);
            ti.add(15);
            ti.add(3);
            ti.add(9);
            ti.add(14);
            ti.add(20);
            ti.add(2);
            ti.add(4);
            ti.add(7);
            ti.add(12);
            ti.add(1);
        } catch(Exception ex) {
            fail();
        }
        assertEquals("10(-1)6(-1)3(-1)2(-1)1(0)---4(0)--9(-1)7(0)---15(-1)14(-1)12(0)---20(0)--", ti.toString());
        assertEquals(5, ti.getHeight());
        
        try {
            ti.remove(20);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(0)3(-1)2(-1)1(0)---4(0)--10(0)9(-1)7(0)---14(0)12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(4);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(1)2(0)1(0)--3(0)--10(0)9(-1)7(0)---14(0)12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(10);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(1)2(0)1(0)--3(0)--9(1)7(0)--14(0)12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(9);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("6(1)2(0)1(0)--3(0)--14(-1)7(1)-12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(6);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("3(1)2(-1)1(0)---14(-1)7(1)-12(0)--15(0)--", ti.toString());
        assertEquals(4, ti.getHeight());
        
        try {
            ti.remove(3);
        } catch (Exception ex) {
            fail();
        }
        assertEquals("7(0)2(-1)1(0)---14(0)12(0)--15(0)--", ti.toString());
        assertEquals(3, ti.getHeight());
    }
    
}
