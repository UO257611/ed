package testsAVL_BST;
import static org.junit.Assert.*;
import org.junit.Test;

import AVL.AVLTree;

public class L8_AVL_sampleTest {
    
    @Test
    public void test_A() {
        // AVLTree before implementing the rotations!
        
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('b');
            a.add('a');
            a.add('d');
            a.add('c');
            a.add('g');
            a.add('i');
            a.add('h');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("b(3)a(0)--d(2)c(0)--g(2)-i(-1)h(0)---", a.toString());
        
    }
    
    @Test
    public void test_B() {
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('a');
            a.add('b');
            a.add('c');
            a.add('d');
            a.add('e');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("b(1)a(0)--d(0)c(0)--e(0)--", a.toString());
        
        try {
            a.add('f');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("d(0)b(0)a(0)--c(0)--e(1)-f(0)--", a.toString());
        
    }
    
    @Test
    public void test_C() {
        AVLTree<Character> a = new AVLTree<Character>();
        
        try {
            a.add('e');
            a.add('g');
            a.add('b');
            a.add('d');
            a.add('c');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("e(-1)c(0)b(0)--d(0)--g(0)--", a.toString());
        
    }
    
    @Test
    public void test_D() {
        AVLTree<Character> a = new AVLTree<Character>();
        try {
            a.add('a');
            a.add('b');
            a.add('d');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("b(0)a(0)--d(0)--", a.toString());
        
        AVLTree<Character> b = new AVLTree<Character>();
        try {
            b.add('c');
            b.add('g');
            b.add('i');
            b.add('d');
        } catch (Exception ex) {
            fail();
        }
        assertEquals("g(-1)c(1)-d(0)--i(0)--", b.toString());
        assertEquals("d(0)b(0)a(0)--c(0)--g(1)-i(0)--", a.joins(b).toString());
        
    }
    
}
