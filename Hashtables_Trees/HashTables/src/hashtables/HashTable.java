package hashtables;

import java.util.ArrayList;

public class HashTable<T  extends Comparable<T>>
{

	public final static int LINEAR_PROBING = 0;
	public final static int QUADRATIC_PROBING = 1;
	private int B = 7; 
	private int redispersionType = LINEAR_PROBING; 
	private double minLF = 0.5 ;
	private ArrayList<HashNode<T>> associativeArray;
	private final static int ATTEMPTS = 25;
	private int n = 0; 
	public final static int DOUBLE_HASHING = 2;
	private int R = 5;

	
	public HashTable(int B, int redispersionType, double minLF)
	{
		if(B > 0 )
		{
			if(redispersionType == LINEAR_PROBING || redispersionType == QUADRATIC_PROBING || redispersionType == DOUBLE_HASHING)
			{		
				if(0 <= minLF && minLF <= 1.0)
				{
					if (isPrime(B))
					{
						this.B = B;
					}
					else
					{
						this.B= getNextPrimeNumber(B);
					}
					this.minLF = minLF;
					this.redispersionType = redispersionType;
					this.R = getPrevPrimeNumber(B);

					
				}
			}
			
		}
		associativeArray = new  ArrayList<HashNode<T>>(this.B);
		for( int i = 0 ; i<this.B; i++)
		{
			associativeArray.add(new HashNode<>());
		}
		
	}
	
	
	/** * Hashing function 
	 * * @param element Element to be stored 
	 * * @param i Attempt number 
	 * * @return Slot in the array where the element should be * placed 
	 * @throws Exception 
	 * */ 
	public int f(T element, int i) 
	{ 
		switch(redispersionType) 
		{ 
		case LINEAR_PROBING: 
			return (Math.abs(element.hashCode()+i) % B);
		case DOUBLE_HASHING: // [x + i*H2(x)] % B
			
			return (Math.abs(element.hashCode())+i*(R-Math.abs(element.hashCode() )% R)) %B;
			
		default: 
			return (int) (Math.abs(element.hashCode()+ Math.pow(i, 2)) % B);
		} 
	}
	
	public String toString()
	{
		String msg = "";
		
		for(int i = 0; i< associativeArray.size() ; i++)
		{
			msg+= "["+i+"] "+ associativeArray.get(i).toString(); 
		}
		return msg;
	}
	
	public double getLF()
	{	
		return (double) n / (double) B;
	}
	
	public void add( T element)
	{
		boolean success = false;	
		int  i = 0;
		
		while(!success && i< ATTEMPTS)
		{
			int position = f(element, i);
			if( associativeArray.get(position).getStatus() != HashNode.VALID )
			{
				associativeArray.get(position).setElement(element);
				associativeArray.get(position).setStatus(HashNode.VALID);;
				success = true;
				n++;
			}
			i++;
	
		}
		if (getLF() > minLF) 
			dynamicResize();

		
		
	}
	
	public boolean search(T element)
	{
		boolean success = false;
		int i = 0;
		
		while (!success && i < ATTEMPTS)
		{
			int pos = f(element, i);
			if(associativeArray.get(pos).getStatus() == HashNode.VALID &&
					associativeArray.get(pos).getElement().compareTo(element)==0)
			{
				success = true;
			}
			else if(associativeArray.get(pos).getStatus() == HashNode.EMPTY )
			{
				break;
			}
			i++;
		}
		return success;
		
	}
	
	public void remove(T element)
	{
		
		boolean success = false;
		int i = 0;
		while (!success && i<ATTEMPTS)
		{
			int pos = f(element, i);
			if(associativeArray.get(pos).getStatus() == HashNode.VALID &&
					associativeArray.get(pos).getElement().compareTo(element)==0)
			{
				associativeArray.get(pos).setStatus(HashNode.DELETED);
				n--;
				success = true;
			}
			i++;
		}
	}
	
	
	public boolean isPrime(int number)
	{
		if( number == 1)
			return false;
		for(int i = 2; i<number; i++)
		{
			if ( number % i == 0)
			{
				return false;
			}
		}
		return true;
		
	}
	
	public int getPrevPrimeNumber(int number) 
	{
		for( int i = number-1; i>1; i--)
		{
			if(isPrime(i))
			{
				return i;
			}
		}
		return -1;
	}
	
	public int getNextPrimeNumber(int number)
	{
		boolean x = true;
		int counter = number+1;
		while(x)
		{
			if(isPrime(counter)) {
				x= false;
				return counter;
			}
			counter++;
		}
		return -1;
	}
	
	
	public ArrayList<HashNode<T>> getAssociativeArray()
	{
		return associativeArray;
	}

	private void dynamicResize (int newSize) 
	{
		
		HashTable<T> newHashTable = new HashTable<T>(newSize, redispersionType, minLF);
		
		for( int i = 0 ; i<associativeArray.size(); i++)
		{
			if( associativeArray.get(i).getStatus()== HashNode.VALID)
				newHashTable.add(associativeArray.get(i).getElement());
		}
		
		this.B =newSize;
		this.associativeArray = newHashTable.getAssociativeArray();
		this.R = getPrevPrimeNumber(B);
		
		
	}
	
	
	

	private void dynamicResize () 
	{
		dynamicResize(getNextPrimeNumber(B*2));
		
		
		
	}
	
	
	
	public int getR()
	{
		return R;
	}
	
	
	


}
