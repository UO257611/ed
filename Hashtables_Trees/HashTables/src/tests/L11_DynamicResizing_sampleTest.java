package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import hashtables.HashTable;

public class L11_DynamicResizing_sampleTest {
    
    @Test
    public void test_A() {
        HashTable<Integer> c = new HashTable<Integer>(5, HashTable.DOUBLE_HASHING, 0.5);
        
        assertEquals(2, c.f(7, 0));
        assertEquals(4, c.f(7, 1));
        assertEquals(1, c.f(7, 2));
        assertEquals(3, c.f(7, 3));
        assertEquals(0, c.f(7, 4));
        
        assertEquals(0, c.f(0, 0));
        assertEquals(1, c.f(2, 4));
        assertEquals(2, c.f(3, 3));
        assertEquals(3, c.f(32, 1));
        assertEquals(4, c.f(1045, 2));
    }
    
    @Test
    public void test_B() {
        HashTable<Character> c = new HashTable<Character>(5, HashTable.DOUBLE_HASHING, 0.5);
        
        assertEquals(0, c.f('A', 0));
        assertEquals(1, c.f('A', 1));
        assertEquals(2, c.f('A', 2));
        assertEquals(3, c.f('A', 3));
        assertEquals(4, c.f('A', 4));
        
        assertEquals(2, c.f('a', 0));
        assertEquals(4, c.f('a', 1));
        assertEquals(1, c.f('a', 2));
        assertEquals(3, c.f('a', 3));
        assertEquals(0, c.f('a', 4));
    }
    
    @Test
    public void test_C() {
        HashTable<Integer> c = new HashTable<Integer>(5, HashTable.DOUBLE_HASHING, 1.0);
        
        c.add(4);
        c.add(13);
        c.add(24);
        c.add(3);
        assertEquals("[0] (0) = null - [1] (1) = 3 - [2] (1) = 24 - [3] (1) = 13 - [4] (1) = 4 - ", c.toString());
        
        c.remove(24);
        assertEquals("[0] (0) = null - [1] (1) = 3 - [2] (2) = 24 - [3] (1) = 13 - [4] (1) = 4 - ", c.toString());
        assertEquals(true, c.search(3));
        
        c.add(15);
        assertEquals(true, c.search(3));
        assertEquals("[0] (1) = 15 - [1] (1) = 3 - [2] (2) = 24 - [3] (1) = 13 - [4] (1) = 4 - ", c.toString());
    }
    
    @Test
    public void test_D() {
        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 0.5);
        
        a.add(4);
        assertEquals(0.2, a.getLF(), 0.1);
        
        a.add(13);
        assertEquals(0.4, a.getLF(), 0.1);
        assertEquals("[0] (0) = null - [1] (0) = null - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
        
        a.add(24); // DYNAMIC RESIZING!
        assertEquals(0.27, a.getLF(), 0.1);
        assertEquals("[0] (0) = null - [1] (0) = null - [2] (1) = 24 - [3] (1) = 13 - [4] (1) = 4 - [5] (0) = null - [6] (0) = null - [7] (0) = null - [8] (0) = null - [9] (0) = null - [10] (0) = null - ", a.toString());
    }
    
}
