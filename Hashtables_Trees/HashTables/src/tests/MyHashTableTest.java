package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import hashtables.HashTable;

public class MyHashTableTest {
	
	HashTable<Integer> a;
	
	@Test
	public void testIsPrime()
	{
		a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
		assertEquals(true, a.isPrime(3));
		assertEquals(true, a.isPrime(7));
		assertEquals(false, a.isPrime(9));
		assertEquals(true, a.isPrime(13));
		assertEquals(true,a.isPrime(409));
		assertEquals(true,a.isPrime(401));
		assertEquals(false,a.isPrime(100));
		
	}
	
	@Test
	public void testGetPrevPrime()
	{
		a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
		try {
		assertEquals(7, a.getPrevPrimeNumber(9));
		assertEquals(2, a.getPrevPrimeNumber(3));
		assertEquals(5, a.getPrevPrimeNumber(7));
		assertEquals(401, a.getPrevPrimeNumber(409));
		}
		catch(Exception e)
		{
			fail();
		}
	}
	
	
	@Test
	public void testGetNextPrime()
	{
		
		a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
		try {
		assertEquals(11, a.getNextPrimeNumber(9));
		assertEquals(5, a.getNextPrimeNumber(3));
		assertEquals(11, a.getNextPrimeNumber(7));
		assertEquals(409, a.getNextPrimeNumber(401));
		}
		catch(Exception e)
		{
			fail();
		}
	}
	
	
	@Test
	public void testAdd()
	{
		a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
		a.add(1);
		a.add(3);
		a.add(2);
		a.add(5);
		
		assertEquals("[0] (1) = 5 - [1] (1) = 1 - [2] (1) = 2 - [3] (1) = 3 - [4] (0) = null - ", a.toString());
		a.add(0);
		assertEquals("[0] (1) = 5 - [1] (1) = 1 - [2] (1) = 2 - [3] (1) = 3 - [4] (1) = 0 - ", a.toString());
		
		a.add(7);
		assertEquals("[0] (1) = 5 - [1] (1) = 1 - [2] (1) = 2 - [3] (1) = 3 - [4] (1) = 0 - ", a.toString());
		
		
	}
	
	
	@Test
	public void testSearch()
	{
		a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
		a.add(1);
		a.add(3);
		a.add(2);
		a.add(5);
		
		assertEquals(true, a.search(1));
		assertEquals(true, a.search(5));
		assertEquals(false, a.search(7));
		
	}
	
	@Test
	public void testRemove()
	{
		a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
		a.add(1);
		a.add(3);
		a.add(2);
		a.add(5);
		
		
		assertEquals("[0] (1) = 5 - [1] (1) = 1 - [2] (1) = 2 - [3] (1) = 3 - [4] (0) = null - ", a.toString());
		
		a.remove(1);
		assertEquals("[0] (1) = 5 - [1] (2) = 1 - [2] (1) = 2 - [3] (1) = 3 - [4] (0) = null - ", a.toString());
		
		a.remove(5);
		assertEquals("[0] (2) = 5 - [1] (2) = 1 - [2] (1) = 2 - [3] (1) = 3 - [4] (0) = null - "
				+ "", a.toString());
		
	}
	
}
