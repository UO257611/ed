package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import hashtables.HashTable;

public class HashTableGivenTests {

	 @Test
	    public void test_A() {
	        
		 	HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 0.5);
	        assertEquals(2, a.f(7, 0));
	        assertEquals(3, a.f(7, 1));
	        assertEquals(4, a.f(7, 2));
	        assertEquals(0, a.f(7, 3));
	        
	        HashTable<Integer> b = new HashTable<Integer>(5, HashTable.QUADRATIC_PROBING, 0.5);
	        assertEquals(2, b.f(7, 0));
	        assertEquals(3, b.f(7, 1));
	        assertEquals(1, b.f(7, 2));
	        assertEquals(1, b.f(7, 3));
	    }
	    
	    @Test
	    public void test_B() {
	        HashTable<Character> a = new HashTable<Character>(5, HashTable.LINEAR_PROBING, 0.5);
	        assertEquals(0, a.f('A', 0));
	        assertEquals(1, a.f('A', 1));
	        assertEquals(2, a.f('A', 2));
	        assertEquals(3, a.f('A', 3));
	        
	        HashTable<Character> b = new HashTable<Character>(5, HashTable.QUADRATIC_PROBING, 0.5);
	        assertEquals(0, b.f('A', 0));
	        assertEquals(1, b.f('A', 1));
	        assertEquals(4, b.f('A', 2));
	        assertEquals(4, b.f('A', 3));
	    }
	    
	    @Test
	    public void test_C() {
	        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
	        a.add(4);
	        a.add(13);
	        a.add(24);
	        a.add(3);
	        
	        assertEquals("[0] (1) = 24 - [1] (1) = 3 - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
	        assertEquals(true, a.search(3));
	        assertEquals(false, a.search(12));
	    }
	  
	    @Test
	    public void test_D() {
	        HashTable<Integer> b = new HashTable<Integer>(5, HashTable.QUADRATIC_PROBING, 1.0);
	        b.add(4);
	        b.add(13);
	        b.add(24);
	        b.add(3);
	        
	        assertEquals("[0] (1) = 24 - [1] (0) = null - [2] (1) = 3 - [3] (1) = 13 - [4] (1) = 4 - ", b.toString());
	        assertEquals(true, b.search(3));
	        assertEquals(false, b.search(12));
	    }
	    
	    @Test
	    public void test_E() {
	        HashTable<Integer> a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
	        
	        a.add(4);
	        a.add(13);
	        a.add(24);
	        a.add(3);
	        
	        a.remove(24);
	        assertEquals(true, a.search(3));
	        assertEquals("[0] (2) = 24 - [1] (1) = 3 - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
	        
	        a.add(15);
	        assertEquals(true, a.search(3));
	        assertEquals("[0] (1) = 15 - [1] (1) = 3 - [2] (0) = null - [3] (1) = 13 - [4] (1) = 4 - ", a.toString());
	        
	    }
	    
	    @Test
	    public void test_F() {
	        HashTable<Integer> b = new HashTable<Integer>(5, HashTable.QUADRATIC_PROBING, 1.0);
	        b.add(4);
	        b.add(13);
	        b.add(24);
	        b.add(3);
	        
	        b.remove(24);
	        assertEquals(true, b.search(3));
	        assertEquals("[0] (2) = 24 - [1] (0) = null - [2] (1) = 3 - [3] (1) = 13 - [4] (1) = 4 - ", b.toString());
	        
	        b.add(15);
	        assertEquals(true, b.search(3));
	        assertEquals("[0] (1) = 15 - [1] (0) = null - [2] (1) = 3 - [3] (1) = 13 - [4] (1) = 4 - ", b.toString());
	    }
	    

}
