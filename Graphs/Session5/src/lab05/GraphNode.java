package lab05;

public class GraphNode<T> 
{
	private T element;
	private boolean visited;
	
	public GraphNode() {
	}
	public GraphNode(T n) {
		element = n;
		visited = false;
	}

	public T getElement() {
		return element;
	}

	public void setElement(T element) {
		this.element = element;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}
	
	public void print() {
		System.out.println(toString());
	}

	public String toString() {
		return element.toString()  ;
		
	}
}
