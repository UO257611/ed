package lab05_test;


import static org.junit.Assert.*;

import org.junit.Test;

import lab05.Graph;

public class L00_Master_Graph_Test {

    @Test
    public void test1_graphEdition_Traverse() {
        Graph<Character> g = new Graph<Character>(3);

        System.out.println("TEST 1 (EDITING) BEGINS ***");
        assertEquals(0, g.getSize());

        try {
            g.addNode('a');
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(1, g.getSize());
        assertEquals(0, g.getNode('a'));
        assertArrayEquals(new boolean[][]{
            {false, false, false},
            {false, false, false},
            {false, false, false}}, g.getEdges());
        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0}}, g.getWeight());

        // Test nodes for nodes not found
        assertEquals(Graph.INDEX_NOT_FOUND, g.getNode('b'));

        // No repeated nodes is allowed
        try {
            g.addNode('a');
            fail();
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        try {
            g.addNode('b');
            g.addNode('c');
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(3, g.getSize());
        assertEquals(0, g.getNode('a'));
        assertEquals(1, g.getNode('b'));
        assertEquals(2, g.getNode('c'));
        //g1.print();

        assertArrayEquals(new boolean[][]{
            {false, false, false},
            {false, false, false},
            {false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0}}, g.getWeight());

        // Testing edges
        try {
            assertEquals(false, g.existsEdge('b', 'd'));
            fail();
        } catch (Exception e) {
            System.out.println("Starting|arrival node does not exists" + e);
        }

        try {
            assertEquals(false, g.existsEdge('b', 'c'));
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, false, false},
            {false, false, false},
            {false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0}}, g.getWeight());

        try {
            g.addEdge('b', 'c', 5.0);
            assertEquals(true, g.existsEdge('b', 'c'));
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, false, false},
            {false, false, true},
            {false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 5.0},
            {0.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("a-", g.traverseGraphDF('a'));
        assertEquals("b-c-", g.traverseGraphDF('b'));
        assertEquals("c-", g.traverseGraphDF('c'));
    }

    @Test
    public void test2_graphEdition_Traverse_Deletion() {
        Graph<String> g = new Graph<String>(5);

        System.out.println("TEST 2 (DELETING) BEGINS ***");
        assertEquals(0, g.getSize());

        try {
            g.addNode("V1");
            g.addNode("V2");
            g.addNode("V3");
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(3, g.getSize());
        assertEquals(0, g.getNode("V1"));
        assertEquals(1, g.getNode("V2"));
        assertEquals(2, g.getNode("V3"));

        assertArrayEquals(new boolean[][]{
            {false, false, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());
        //g.print();

        try {
            g.addEdge("V1", "V2", 2.0);
            g.addEdge("V1", "V3", 1.0);
            g.addEdge("V3", "V2", 4.0);
            assertEquals(true, g.existsEdge("V1", "V2"));
            assertEquals(true, g.existsEdge("V1", "V3"));
            assertEquals(true, g.existsEdge("V3", "V2"));
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, true, true, false, false},
            {false, false, false, false, false},
            {false, true, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 2.0, 1.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 4.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("V1-V2-V3-", g.traverseGraphDF("V1"));
        assertEquals("V2-", g.traverseGraphDF("V2"));
        assertEquals("V3-V2-", g.traverseGraphDF("V3"));

        //g.print();
        // Adds V4
        try {
            g.addNode("V4");

        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(4, g.getSize());
        assertEquals(3, g.getNode("V4"));

        assertArrayEquals(new boolean[][]{
            {false, true, true, false, false},
            {false, false, false, false, false},
            {false, true, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 2.0, 1.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 4.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("V1-V2-V3-", g.traverseGraphDF("V1"));
        assertEquals("V2-", g.traverseGraphDF("V2"));
        assertEquals("V3-V2-", g.traverseGraphDF("V3"));
        assertEquals("V4-", g.traverseGraphDF("V4"));

        // Deletes V2
        try {
            g.removeNode("V2");

        } catch (Exception e) {
            System.out.println("The element to be deleted does not exist: " + e);
        }

        assertEquals(3, g.getSize());
        assertEquals(1, g.getNode("V4"));
        assertArrayEquals(new boolean[][]{
            {false, false, true, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 1.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("V1-V3-", g.traverseGraphDF("V1"));
        assertEquals("V3-", g.traverseGraphDF("V3"));
        assertEquals("V4-", g.traverseGraphDF("V4"));
    }

    @Test
    public void test3_Floyd() {
        Graph<String> g = new Graph<String>(6);

        System.out.println("TEST 3 (FLOYD EXERCISE) BEGINS ***");
        assertEquals(0, g.getSize());

        try {
            g.addNode("V1");
            g.addNode("V2");
            g.addNode("V3");
            g.addNode("V4");
            g.addNode("V5");
            g.addNode("V6");
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(6, g.getSize());
        assertEquals(0, g.getNode("V1"));
        assertEquals(1, g.getNode("V2"));
        assertEquals(2, g.getNode("V3"));
        assertEquals(3, g.getNode("V4"));
        assertEquals(4, g.getNode("V5"));
        assertEquals(5, g.getNode("V6"));

        assertArrayEquals(new boolean[][]{
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        try {
            g.addEdge("V1", "V2", 3.0);
            g.addEdge("V1", "V3", 4.0);
            g.addEdge("V1", "V5", 8.0);
            g.addEdge("V2", "V5", 5.0);
            g.addEdge("V3", "V5", 3.0);
            g.addEdge("V5", "V6", 3.0);
            g.addEdge("V5", "V4", 7.0);
            g.addEdge("V6", "V4", 2.0);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, true, true, false, true, false},
            {false, false, false, false, true, false},
            {false, false, false, false, true, false},
            {false, false, false, false, false, false},
            {false, false, false, true, false, true},
            {false, false, false, true, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 3.0, 4.0, 0.0, 8.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 5.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 3.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 7.0, 0.0, 3.0},
            {0.0, 0.0, 0.0, 2.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("V1-V2-V5-V4-V6-V3-", g.traverseGraphDF("V1"));
        assertEquals("V2-V5-V4-V6-", g.traverseGraphDF("V2"));
        assertEquals("V3-V5-V4-V6-", g.traverseGraphDF("V3"));
        assertEquals("V4-", g.traverseGraphDF("V4"));
        assertEquals("V5-V4-V6-", g.traverseGraphDF("V5"));
        assertEquals("V6-V4-", g.traverseGraphDF("V6"));

        g.floyd(g.getSize());

        assertArrayEquals(new int[][]{
            {-1, -1, -1, 5, 2, 4},
            {-1, -1, -1, 5, -1, 4},
            {-1, -1, -1, 5, -1, 4},
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -1, 5, -1, -1},
            {-1, -1, -1, -1, -1, -1}}, g.getP());

        assertArrayEquals(new double[][]{
            {00.0, 03.0, 04.0, 12.0, 07.0, 10.0},
            {Graph.INFINITE, 00.0, Graph.INFINITE, 10.0, 05.0, 08.0},
            {Graph.INFINITE, Graph.INFINITE, 00.0, 08.0, 03.0, 06.0},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 00.0, Graph.INFINITE, Graph.INFINITE},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 05.0, 00.0, 03.0},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 02.0, Graph.INFINITE, 00.0}}, g.getA());

        try {
            assertEquals("V1V2", "V1" + g.printFloydPath("V1", "V2") + "V2");
            assertEquals(3.0, g.getA()[g.getNode("V1")][g.getNode("V2")], 0.01);
            assertEquals("V1V3", "V1" + g.printFloydPath("V1", "V3") + "V3");
            assertEquals(4.0, g.getA()[g.getNode("V1")][g.getNode("V3")], 0.01);
            assertEquals("V1V3V5V6V4", "V1" + g.printFloydPath("V1", "V4") + "V4");
            assertEquals(12.0, g.getA()[g.getNode("V1")][g.getNode("V4")], 0.01);
            assertEquals("V1V3V5", "V1" + g.printFloydPath("V1", "V5") + "V5");
            assertEquals(7.0, g.getA()[g.getNode("V1")][g.getNode("V5")], 0.01);
            assertEquals("V1V3V5V6", "V1" + g.printFloydPath("V1", "V6") + "V6");
            assertEquals(10.0, g.getA()[g.getNode("V1")][g.getNode("V6")], 0.01);

            assertEquals("V2V1", "V2" + g.printFloydPath("V2", "V1") + "V1");
            assertEquals(Graph.INFINITE, g.getA()[g.getNode("V2")][g.getNode("V1")], 0.01);
            assertEquals("V2V3", "V2" + g.printFloydPath("V2", "V3") + "V3");
            assertEquals(Graph.INFINITE, g.getA()[g.getNode("V2")][g.getNode("V3")], 0.01);
            assertEquals("V2V5V6V4", "V2" + g.printFloydPath("V2", "V4") + "V4");
            assertEquals(10, g.getA()[g.getNode("V2")][g.getNode("V4")], 0.01);
            assertEquals("V2V5", "V2" + g.printFloydPath("V2", "V5") + "V5");
            assertEquals(5.0, g.getA()[g.getNode("V2")][g.getNode("V5")], 0.01);
            assertEquals("V2V5V6", "V2" + g.printFloydPath("V2", "V6") + "V6");
            assertEquals(8.0, g.getA()[g.getNode("V2")][g.getNode("V6")], 0.01);

        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }
    }

    @Test
    public void test4_Dijkstra() {
        Graph<String> g = new Graph<String>(6);

        System.out.println("TEST 4 (DIJKSTRA EXERCISE) BEGINS ***");
        assertEquals(0, g.getSize());

        try {
            g.addNode("V1");
            g.addNode("V2");
            g.addNode("V3");
            g.addNode("V4");
            g.addNode("V5");
            g.addNode("V6");
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(6, g.getSize());
        assertEquals(0, g.getNode("V1"));
        assertEquals(1, g.getNode("V2"));
        assertEquals(2, g.getNode("V3"));
        assertEquals(3, g.getNode("V4"));
        assertEquals(4, g.getNode("V5"));
        assertEquals(5, g.getNode("V6"));

        assertArrayEquals(new boolean[][]{
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false},
            {false, false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        try {
            g.addEdge("V1", "V2", 3.0);
            g.addEdge("V1", "V3", 4.0);
            g.addEdge("V1", "V5", 8.0);
            g.addEdge("V2", "V5", 5.0);
            g.addEdge("V3", "V5", 3.0);
            g.addEdge("V5", "V6", 3.0);
            g.addEdge("V5", "V4", 7.0);
            g.addEdge("V6", "V4", 2.0);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, true, true, false, true, false},
            {false, false, false, false, true, false},
            {false, false, false, false, true, false},
            {false, false, false, false, false, false},
            {false, false, false, true, false, true},
            {false, false, false, true, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 3.0, 4.0, 0.0, 8.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 5.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 3.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 7.0, 0.0, 3.0},
            {0.0, 0.0, 0.0, 2.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("V1-V2-V5-V4-V6-V3-", g.traverseGraphDF("V1"));
        assertEquals("V2-V5-V4-V6-", g.traverseGraphDF("V2"));
        assertEquals("V3-V5-V4-V6-", g.traverseGraphDF("V3"));
        assertEquals("V4-", g.traverseGraphDF("V4"));
        assertEquals("V5-V4-V6-", g.traverseGraphDF("V5"));
        assertEquals("V6-V4-", g.traverseGraphDF("V6"));

        g.Dijkstra("V1");

        assertArrayEquals(new double[][]{{Graph.INFINITE, 3, 4, 12.0, 7.0, 10.0}}, g.getD());
        assertArrayEquals(new int[]{-1, 0, 0, 5, 2, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath("V1", "V1"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V1")], 0.01);
            assertEquals("V1V2", g.printDijkstraPath("V1", "V2"));
            assertEquals(3, g.getD()[0][g.getNode("V2")], 0.01);
            assertEquals("V1V3", g.printDijkstraPath("V1", "V3"));
            assertEquals(4, g.getD()[0][g.getNode("V3")], 0.01);
            assertEquals("V1V3V5V6V4", g.printDijkstraPath("V1", "V4"));
            assertEquals(12.0, g.getD()[0][g.getNode("V4")], 0.01);
            assertEquals("V1V3V5", g.printDijkstraPath("V1", "V5"));
            assertEquals(7.0, g.getD()[0][g.getNode("V5")], 0.01);
            assertEquals("V1V3V5V6", g.printDijkstraPath("V1", "V6"));
            assertEquals(10.0, g.getD()[0][g.getNode("V6")], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra("V2");

        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 10.0, 5.0, 8.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, 5, 1, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath("V2", "V1"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V1")], 0.01);
            assertEquals("", g.printDijkstraPath("V2", "V2"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V2")], 0.01);
            assertEquals("", g.printDijkstraPath("V2", "V3"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V3")], 0.01);
            assertEquals("V2V5V6V4", g.printDijkstraPath("V2", "V4"));
            assertEquals(10.0, g.getD()[0][g.getNode("V4")], 0.01);
            assertEquals("V2V5", g.printDijkstraPath("V2", "V5"));
            assertEquals(5.0, g.getD()[0][g.getNode("V5")], 0.01);
            assertEquals("V2V5V6", g.printDijkstraPath("V2", "V6"));
            assertEquals(8.0, g.getD()[0][g.getNode("V6")], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra("V3");
        

        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 8.0, 3.0, 6.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, 5, 2, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath("V3", "V1"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V1")], 0.01);
            assertEquals("", g.printDijkstraPath("V3", "V2"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V2")], 0.01);
            assertEquals("", g.printDijkstraPath("V3", "V3"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V3")], 0.01);
            assertEquals("V3V5V6V4", g.printDijkstraPath("V3", "V4"));
            assertEquals(8.0, g.getD()[0][g.getNode("V4")], 0.01);
            assertEquals("V3V5", g.printDijkstraPath("V3", "V5"));
            assertEquals(3.0, g.getD()[0][g.getNode("V5")], 0.01);
            assertEquals("V3V5V6", g.printDijkstraPath("V3", "V6"));
            assertEquals(6.0, g.getD()[0][g.getNode("V6")], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra("V4");

        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, -1, -1}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath("V4", "V1"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V1")], 0.01);
            assertEquals("", g.printDijkstraPath("V4", "V2"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V2")], 0.01);
            assertEquals("", g.printDijkstraPath("V4", "V3"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V3")], 0.01);
            assertEquals("", g.printDijkstraPath("V4", "V4"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V4")], 0.01);
            assertEquals("", g.printDijkstraPath("V4", "V5"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V5")], 0.01);
            assertEquals("", g.printDijkstraPath("V4", "V6"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V6")], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra("V5");

        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 5.0, Graph.INFINITE, 3.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, 5, -1, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath("V5", "V1"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V1")], 0.01);
            assertEquals("", g.printDijkstraPath("V5", "V2"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V2")], 0.01);
            assertEquals("", g.printDijkstraPath("V5", "V3"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V3")], 0.01);
            assertEquals("V5V6V4", g.printDijkstraPath("V5", "V4"));
            assertEquals(5.0, g.getD()[0][g.getNode("V4")], 0.01);
            assertEquals("", g.printDijkstraPath("V5", "V5"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V5")], 0.01);
            assertEquals("V5V6", g.printDijkstraPath("V5", "V6"));
            assertEquals(3, g.getD()[0][g.getNode("V6")], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra("V6");

        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 2.0, Graph.INFINITE, Graph.INFINITE}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, 5, -1, -1}, g.getPD());

        try {
           /* assertEquals("", g.printDijkstraPath("V6", "V1"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V1")], 0.01);
            assertEquals("", g.printDijkstraPath("V6", "V2"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V2")], 0.01);
            assertEquals("", g.printDijkstraPath("V6", "V3"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V3")], 0.01);*/
            assertEquals("V6V4", g.printDijkstraPath("V6", "V4"));
            assertEquals(2.0, g.getD()[0][g.getNode("V4")], 0.01);
            assertEquals("", g.printDijkstraPath("V6", "V5"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V5")], 0.01);
            assertEquals("", g.printDijkstraPath("V6", "V6"));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode("V6")], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

    }

    @Test
    public void test5_Dijkstra_Floyd() {
        Graph<Integer> g = new Graph<Integer>(7);

        System.out.println("TEST 5 (DIJKSTRA FLOYD EXERCISE) BEGINS ***");
        assertEquals(0, g.getSize());

        try {
            g.addNode(1);
            g.addNode(2);
            g.addNode(3);
            g.addNode(4);
            g.addNode(5);
            g.addNode(6);
            g.addNode(7);
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(7, g.getSize());
        assertEquals(0, g.getNode(1));
        assertEquals(1, g.getNode(2));
        assertEquals(2, g.getNode(3));
        assertEquals(3, g.getNode(4));
        assertEquals(4, g.getNode(5));
        assertEquals(5, g.getNode(6));
        assertEquals(6, g.getNode(7));

        assertArrayEquals(new boolean[][]{
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        try {
            g.addEdge(1, 2, 3.0);
            g.addEdge(1, 3, 2.0);
            g.addEdge(2, 3, 1.0);
            g.addEdge(2, 4, 2.0);
            g.addEdge(2, 5, 2.0);
            g.addEdge(3, 6, 1.0);
            g.addEdge(4, 3, 1.0);
            g.addEdge(4, 5, 1.0);
            g.addEdge(5, 7, 2.0);
            g.addEdge(6, 4, 3.0);
            g.addEdge(6, 5, 3.0);
            g.addEdge(6, 7, 7.0);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, true, true, false, false, false, false},
            {false, false, true, true, true, false, false},
            {false, false, false, false, false, true, false},
            {false, false, true, false, true, false, false},
            {false, false, false, false, false, false, true},
            {false, false, false, true, true, false, true},
            {false, false, false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 3.0, 2.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 1.0, 2.0, 2.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0},
            {0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0},
            {0.0, 0.0, 0.0, 3.0, 3.0, 0.0, 7.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("1-2-3-6-4-5-7-", g.traverseGraphDF(1));
        assertEquals("2-3-6-4-5-7-", g.traverseGraphDF(2));
        assertEquals("3-6-4-5-7-", g.traverseGraphDF(3));
        assertEquals("4-3-6-5-7-", g.traverseGraphDF(4));
        assertEquals("5-7-", g.traverseGraphDF(5));
        assertEquals("6-4-3-5-7-", g.traverseGraphDF(6));

        g.floyd(7);

        assertArrayEquals(new double[][]{
            {00.0, 3.00, 2.00, 5.00, 5.00, 3.00, 7.00},
            {Graph.INFINITE, 00.0, 1.00, 2.00, 2.00, 2.00, 4.00},
            {Graph.INFINITE, Graph.INFINITE, 0.00, 4.00, 4.00, 1.00, 6.00},
            {Graph.INFINITE, Graph.INFINITE, 1.00, 0.00, 1.00, 2.00, 3.00},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 0.0, Graph.INFINITE, 2.00},
            {Graph.INFINITE, Graph.INFINITE, 4.00, 3.00, 3.00, 0.00, 5.00},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 00.0}}, g.getA());

        assertArrayEquals(new int[][]{
            {-1, -1, -1, 1, 1, 2, 4},
            {-1, -1, -1, -1, -1, 2, 4},
            {-1, -1, -1, 5, 5, -1, 5},
            {-1, -1, -1, -1, -1, 2, 4},
            {-1, -1, -1, -1, -1, -1, -1},
            {-1, -1, 3, -1, -1, -1, 4},
            {-1, -1, -1, -1, -1, -1, -1},}, g.getP());

        try {
            assertEquals("12", "1" + g.printFloydPath(1, 2) + "2");
            assertEquals(3.0, g.getA()[g.getNode(1)][g.getNode(2)], 0.01);
            assertEquals("13", "1" + g.printFloydPath(1, 3) + "3");
            assertEquals(2.0, g.getA()[g.getNode(1)][g.getNode(3)], 0.01);
            assertEquals("124", "1" + g.printFloydPath(1, 4) + "4");
            assertEquals(5.0, g.getA()[g.getNode(1)][g.getNode(4)], 0.01);
            assertEquals("125", "1" + g.printFloydPath(1, 5) + "5");
            assertEquals(5.0, g.getA()[g.getNode(1)][g.getNode(5)], 0.01);
            assertEquals("136", "1" + g.printFloydPath(1, 6) + "6");
            assertEquals(3.0, g.getA()[g.getNode(1)][g.getNode(6)], 0.01);
            assertEquals("137", "1" + g.printFloydPath(1, 6) + "7");
            assertEquals(7.0, g.getA()[g.getNode(1)][g.getNode(7)], 0.01);

            assertEquals("21", "2" + g.printFloydPath(2, 1) + "1");
            assertEquals(Graph.INFINITE, g.getA()[g.getNode(2)][g.getNode(1)], 0.01);
            assertEquals("23", "2" + g.printFloydPath(2, 3) + "3");
            assertEquals(1, g.getA()[g.getNode(2)][g.getNode(3)], 0.01);
            assertEquals("24", "2" + g.printFloydPath(2, 4) + "4");
            assertEquals(2.0, g.getA()[g.getNode(2)][g.getNode(4)], 0.01);
            assertEquals("25", "2" + g.printFloydPath(2, 5) + "5");
            assertEquals(2.0, g.getA()[g.getNode(2)][g.getNode(5)], 0.01);
            assertEquals("236", "2" + g.printFloydPath(2, 6) + "6");
            assertEquals(2.0, g.getA()[g.getNode(2)][g.getNode(6)], 0.01);
            assertEquals("257", "2" + g.printFloydPath(2, 7) + "7");
            assertEquals(4.0, g.getA()[g.getNode(2)][g.getNode(7)], 0.01);

        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(1);

        assertArrayEquals(new double[][]{{Graph.INFINITE, 3.0, 2.0, 5.0, 5.0, 3.0, 7.0}}, g.getD());
        assertArrayEquals(new int[]{-1, 0, 0, 1, 1, 2, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath(1, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("12", g.printDijkstraPath(1, 2));
            assertEquals(3.0, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("13", g.printDijkstraPath(1, 3));
            assertEquals(2.0, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("124", g.printDijkstraPath(1, 4));
            assertEquals(5.0, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("125", g.printDijkstraPath(1, 5));
            assertEquals(5.0, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("136", g.printDijkstraPath(1, 6));
            assertEquals(3.0, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("1257", g.printDijkstraPath(1, 7));
            assertEquals(7.0, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(2);
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, 1.0, 2.0, 2.0, 2.0, 4.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, 1, 1, 1, 2, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath(2, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("", g.printDijkstraPath(2, 2));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("23", g.printDijkstraPath(2, 3));
            assertEquals(1.0, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("24", g.printDijkstraPath(2, 4));
            assertEquals(2.0, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("25", g.printDijkstraPath(2, 5));
            assertEquals(2.0, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("236", g.printDijkstraPath(2, 6));
            assertEquals(2.0, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("257", g.printDijkstraPath(2, 7));
            assertEquals(4.0, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(3);
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, 5.0, 4.0, 4.0, 1.0, 6.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, 3, 5, 5, 2, 4}, g.getPD());
        try {
            assertEquals("", g.printDijkstraPath(3, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("", g.printDijkstraPath(3, 2));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("3643", g.printDijkstraPath(3, 3));
            assertEquals(5.0, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("364", g.printDijkstraPath(3, 4));
            assertEquals(4.0, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("365", g.printDijkstraPath(3, 5));
            assertEquals(4.0, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("36", g.printDijkstraPath(3, 6));
            assertEquals(1.0, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("3657", g.printDijkstraPath(3, 7));
            assertEquals(6.0, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(4);
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, 1.0, 5.0, 1.0, 2.0, 3.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, 3, 5, 3, 2, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath(4, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("", g.printDijkstraPath(4, 2));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("43", g.printDijkstraPath(4, 3));
            assertEquals(1.0, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("4364", g.printDijkstraPath(4, 4));
            assertEquals(5.0, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("45", g.printDijkstraPath(4, 5));
            assertEquals(1.0, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("436", g.printDijkstraPath(4, 6));
            assertEquals(2.0, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("457", g.printDijkstraPath(4, 7));
            assertEquals(3.0, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(5);
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 2.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, -1, -1, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath(5, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("", g.printDijkstraPath(5, 2));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("", g.printDijkstraPath(5, 3));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("", g.printDijkstraPath(5, 4));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("", g.printDijkstraPath(5, 5));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("", g.printDijkstraPath(5, 6));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("57", g.printDijkstraPath(5, 7));
            assertEquals(2.0, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(6);
        g.printDijkstra();
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, 4.0, 3.0, 3.0, 5.0, 5.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, 3, 5, 5, 2, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath(6, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("", g.printDijkstraPath(6, 2));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("643", g.printDijkstraPath(6, 3));
            assertEquals(4.0, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("64", g.printDijkstraPath(6, 4));
            assertEquals(3.0, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("65", g.printDijkstraPath(6, 5));
            assertEquals(3.0, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("6436", g.printDijkstraPath(6, 6));
            assertEquals(5.0, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("657", g.printDijkstraPath(6, 7));
            assertEquals(5.0, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra(7);
        g.printDijkstra();
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, -1, -1, -1}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath(7, 1));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(1)], 0.01);
            assertEquals("", g.printDijkstraPath(7, 2));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(2)], 0.01);
            assertEquals("", g.printDijkstraPath(7, 3));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(3)], 0.01);
            assertEquals("", g.printDijkstraPath(7, 4));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(4)], 0.01);
            assertEquals("", g.printDijkstraPath(7, 5));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(5)], 0.01);
            assertEquals("", g.printDijkstraPath(7, 6));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(6)], 0.01);
            assertEquals("", g.printDijkstraPath(7, 7));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode(7)], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }
    }

    @Test
    public void test6_Dijkstra_Floyd() {
        Graph<Character> g = new Graph<Character>(7);

        System.out.println("TEST 6 (DIJKSTRA FLOYD EXERCISE) BEGINS ***");
        assertEquals(0, g.getSize());

        try {
            g.addNode('a');
            g.addNode('b');
            g.addNode('c');
            g.addNode('d');
            g.addNode('e');
            g.addNode('f');
            g.addNode('g');
        } catch (Exception e) {
            System.out.println("No repeated nodes are allowed" + e);
        }

        assertEquals(7, g.getSize());
        assertEquals(0, g.getNode('a'));
        assertEquals(1, g.getNode('b'));
        assertEquals(2, g.getNode('c'));
        assertEquals(3, g.getNode('d'));
        assertEquals(4, g.getNode('e'));
        assertEquals(5, g.getNode('f'));
        assertEquals(6, g.getNode('g'));

        assertArrayEquals(new boolean[][]{
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false},
            {false, false, false, false, false, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}, g.getWeight());
        // g.print();

        try {
            g.addEdge('a', 'b', 4);
            g.addEdge('a', 'c', 4);
            g.addEdge('b', 'd', 1);
            g.addEdge('b', 'f', 2);
            g.addEdge('b', 'e', 2);
            g.addEdge('c', 'e', 3);
            g.addEdge('d', 'f', 1);
            g.addEdge('e', 'f', 1);
            g.addEdge('e', 'g', 1);
            g.addEdge('f', 'g', 1);
            g.addEdge('g', 'e', 1);

        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        assertArrayEquals(new boolean[][]{
            {false, true, true, false, false, false, false},
            {false, false, false, true, true, true, false},
            {false, false, false, false, true, false, false},
            {false, false, false, false, false, true, false},
            {false, false, false, false, false, true, true},
            {false, false, false, false, false, false, true},
            {false, false, false, false, true, false, false}}, g.getEdges());

        assertArrayEquals(new double[][]{
            {0.0, 4.0, 4.0, 0.0, 0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 1.0, 2.0, 2.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 3.0, 0.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0},
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0},
            {0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0}}, g.getWeight());

        assertEquals("a-b-d-f-g-e-c-", g.traverseGraphDF('a'));
        assertEquals("b-d-f-g-e-", g.traverseGraphDF('b'));
        assertEquals("c-e-f-g-", g.traverseGraphDF('c'));
        assertEquals("d-f-g-e-", g.traverseGraphDF('d'));
        assertEquals("e-f-g-", g.traverseGraphDF('e'));
        assertEquals("f-g-e-", g.traverseGraphDF('f'));
        assertEquals("g-e-f-", g.traverseGraphDF('g'));

        g.floyd(7);

        assertArrayEquals(new double[][]{
            {00.0, 04.0, 04.0, 05.0, 06.0, 06.0, 07.0},
            {Graph.INFINITE, 00.0, Graph.INFINITE, 01.0, 02.0, 02.0, 03.0},
            {Graph.INFINITE, Graph.INFINITE, 00.0, Graph.INFINITE, 3.0, 4.0, 4.0},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 00.0, 3.0, 1.0, 2.0},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 0.0, 1.0, 1.0},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 2.0, 0.0, 1.0},
            {Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 1.0, 2.0, 0.0}}, g.getA());

        assertArrayEquals(new int[][]{
            {-1, -1, -1, 1, 1, 1, 4},
            {-1, -1, -1, -1, -1, -1, 4},
            {-1, -1, -1, -1, -1, 4, 4},
            {-1, -1, -1, -1, 6, -1, 5},
            {-1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, 6, -1, -1},
            {-1, -1, -1, -1, -1, 4, -1},}, g.getP());

        try {
            assertEquals("abeg", 'a' + g.printFloydPath('a', 'g') + 'g');
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        try {
            assertEquals("aa", 'a' + g.printFloydPath('a', 'a') + 'a');
            assertEquals(0.0, g.getA()[g.getNode('a')][g.getNode('a')], 0.01);
            assertEquals("ab", 'a' + g.printFloydPath('a', 'b') + 'b');
            assertEquals(4.0, g.getA()[g.getNode('a')][g.getNode('b')], 0.01);
            assertEquals("ac", 'a' + g.printFloydPath('a', 'c') + 'c');
            assertEquals(4.0, g.getA()[g.getNode('a')][g.getNode('c')], 0.01);
            assertEquals("abd", 'a' + g.printFloydPath('a', 'd') + 'd');
            assertEquals(5.0, g.getA()[g.getNode('a')][g.getNode('d')], 0.01);
            assertEquals("abe", 'a' + g.printFloydPath('a', 'e') + 'e');
            assertEquals(6.0, g.getA()[g.getNode('a')][g.getNode('e')], 0.01);
            assertEquals("abf", 'a' + g.printFloydPath('a', 'f') + 'f');
            assertEquals(6.0, g.getA()[g.getNode('a')][g.getNode('f')], 0.01);
            assertEquals("abeg", 'a' + g.printFloydPath('a', 'g') + 'g');
            assertEquals(7.0, g.getA()[g.getNode('a')][g.getNode('g')], 0.01);

            assertEquals(Graph.INFINITE, g.getA()[g.getNode('b')][g.getNode('a')], 0.01);
            assertEquals("bb", 'b' + g.printFloydPath('b', 'b') + 'b');
            assertEquals(0.0, g.getA()[g.getNode('b')][g.getNode('b')], 0.01);

            assertEquals(Graph.INFINITE, g.getA()[g.getNode('b')][g.getNode('c')], 0.01);
            assertEquals("bd", 'b' + g.printFloydPath('b', 'd') + 'd');
            assertEquals(1.0, g.getA()[g.getNode('b')][g.getNode('d')], 0.01);
            assertEquals("be", 'b' + g.printFloydPath('b', 'e') + 'e');
            assertEquals(2.0, g.getA()[g.getNode('b')][g.getNode('e')], 0.01);
            assertEquals("bf", 'b' + g.printFloydPath('b', 'f') + 'f');
            assertEquals(2.0, g.getA()[g.getNode('b')][g.getNode('f')], 0.01);
            assertEquals("beg", 'b' + g.printFloydPath('b', 'g') + 'g');
            assertEquals(3.0, g.getA()[g.getNode('b')][g.getNode('g')], 0.01);

        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('a');
        assertArrayEquals(new double[][]{{Graph.INFINITE, 4.0, 4.0, 5.0, 6.0, 6.0, 7.0}}, g.getD());
        assertArrayEquals(new int[]{-1, 0, 0, 1, 1, 1, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath('a', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("ab", g.printDijkstraPath('a', 'b'));
            assertEquals(4, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("ac", g.printDijkstraPath('a', 'c'));
            assertEquals(4.0, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("abd", g.printDijkstraPath('a', 'd'));
            assertEquals(5.0, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("abe", g.printDijkstraPath('a', 'e'));
            assertEquals(6.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("abf", g.printDijkstraPath('a', 'f'));
            assertEquals(6.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("abeg", g.printDijkstraPath('a', 'g'));
            assertEquals(7.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('b');
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 1.0, 2.0, 2.0, 3.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, 1, 1, 1, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath('b', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("", g.printDijkstraPath('b', 'b'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("", g.printDijkstraPath('b', 'c'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("bd", g.printDijkstraPath('b', 'd'));
            assertEquals(1.0, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("be", g.printDijkstraPath('b', 'e'));
            assertEquals(2.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("bf", g.printDijkstraPath('b', 'f'));
            assertEquals(2.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("beg", g.printDijkstraPath('b', 'g'));
            assertEquals(3.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('c');
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 3.0, 4.0, 4.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, 2, 4, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath('c', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("", g.printDijkstraPath('c', 'b'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("", g.printDijkstraPath('c', 'c'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("", g.printDijkstraPath('c', 'd'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("ce", g.printDijkstraPath('c', 'e'));
            assertEquals(3.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("cef", g.printDijkstraPath('c', 'f'));
            assertEquals(4.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("ceg", g.printDijkstraPath('c', 'g'));
            assertEquals(4.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('d');
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 3.0, 1.0, 2.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, 6, 3, 5}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath('d', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("", g.printDijkstraPath('d', 'b'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("", g.printDijkstraPath('d', 'c'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("", g.printDijkstraPath('d', 'd'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("dfge", g.printDijkstraPath('d', 'e'));
            assertEquals(3.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("df", g.printDijkstraPath('d', 'f'));
            assertEquals(1.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("dfg", g.printDijkstraPath('d', 'g'));
            assertEquals(2.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('e');
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 2.0, 1.0, 1.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, 6, 4, 4}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath('e', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("", g.printDijkstraPath('e', 'b'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("", g.printDijkstraPath('e', 'c'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("", g.printDijkstraPath('e', 'd'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("ege", g.printDijkstraPath('e', 'e'));
            assertEquals(2.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("ef", g.printDijkstraPath('e', 'f'));
            assertEquals(1.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("eg", g.printDijkstraPath('e', 'g'));
            assertEquals(1.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('f');
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 2.0, 3.0, 1.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, 6, 4, 5}, g.getPD());

        try {
            assertEquals("", g.printDijkstraPath('f', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("", g.printDijkstraPath('f', 'b'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("", g.printDijkstraPath('f', 'c'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("", g.printDijkstraPath('f', 'd'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("fge", g.printDijkstraPath('f', 'e'));
            assertEquals(2.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("fgef", g.printDijkstraPath('f', 'f'));
            assertEquals(3.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("fg", g.printDijkstraPath('f', 'g'));
            assertEquals(1.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }

        g.Dijkstra('g');
        assertArrayEquals(new double[][]{{Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, Graph.INFINITE, 1.0, 2.0, 2.0}}, g.getD());
        assertArrayEquals(new int[]{-1, -1, -1, -1, 6, 4, 4}, g.getPD());
        System.out.println();

        try {
            assertEquals("", g.printDijkstraPath('g', 'a'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('a')], 0.01);
            assertEquals("", g.printDijkstraPath('g', 'b'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('b')], 0.01);
            assertEquals("", g.printDijkstraPath('g', 'c'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('c')], 0.01);
            assertEquals("", g.printDijkstraPath('g', 'd'));
            assertEquals(Graph.INFINITE, g.getD()[0][g.getNode('d')], 0.01);
            assertEquals("ge", g.printDijkstraPath('g', 'e'));
            assertEquals(1.0, g.getD()[0][g.getNode('e')], 0.01);
            assertEquals("gef", g.printDijkstraPath('g', 'f'));
            assertEquals(2.0, g.getD()[0][g.getNode('f')], 0.01);
            assertEquals("geg", g.printDijkstraPath('g', 'g'));
            assertEquals(2.0, g.getD()[0][g.getNode('g')], 0.01);
        } catch (Exception e) {
            System.out.println("Starting or arrival node does not exists" + e);
        }
    }

}
