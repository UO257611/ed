package lab05_test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GraphNodeTest.class, L00_Master_Graph_Test.class, L00_Master_Graph_Test_Martins_Class.class,
		L5_Graph_Dijkstra_sampleTest.class })
public class AllTests {

}
