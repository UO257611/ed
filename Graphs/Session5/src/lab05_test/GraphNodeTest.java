package lab05_test;
import static org.junit.Assert.*;

import org.junit.Test;

import lab05.Graph;

import org.junit.Before;
import org.junit.Test;

public class GraphNodeTest {
	Graph graph;
	Graph dijkstra;
	Graph graph2;
	
	@Before
	public void setUp() throws Exception {
	    graph = new Graph(6);
		
		
		graph.addNode('a');
		graph.addNode('b');
		graph.addNode('c');
		graph.addNode('h');
		graph.addNode('d');
		graph.addNode('r');
		graph.addEdge('a', 'c', 5);
		graph.addEdge('h', 'c', 5);
		graph.addEdge('b', 'a', 5);
		graph.addEdge('h', 'a', 5);
		graph.addEdge('c','d', 7);
		graph.addEdge('h','r', 1);
		
		dijkstra = new Graph(6);
		dijkstra.addNode("v1");
		dijkstra.addNode("v2");
		dijkstra.addNode("v3");
		dijkstra.addNode("v4");
		dijkstra.addNode("v5");
		dijkstra.addNode("v6");
		dijkstra.addEdge("v1","v3",4);
		dijkstra.addEdge("v1","v5",8);
		dijkstra.addEdge("v1","v2",3);
		dijkstra.addEdge("v3","v5",3);
		dijkstra.addEdge("v5","v6",3);
		dijkstra.addEdge("v5","v4",7);
		dijkstra.addEdge("v6","v4",2);
		dijkstra.addEdge("v2","v5",5);
		
		graph2= new Graph(5);
		graph2.addNode("v1");
		graph2.addNode("v2");
		graph2.addNode("v3");
		graph2.addNode("v4");
		graph2.addNode("v5");
		graph2.addEdge("v1", "v5", 10);
		graph2.addEdge("v1", "v4", 3);
		graph2.addEdge("v1", "v2", 1);
		graph2.addEdge("v2", "v3", 5);
		graph2.addEdge("v3", "v5", 1);
		graph2.addEdge("v4", "v5", 6);
		graph2.addEdge("v4", "v3", 2);

		
		
	}

	@Test
	public void traverseTest() throws Exception {
		
		
		assertEquals("a-c-d-", graph.traverseGraphDF('a'));
		assertEquals("b-a-c-d-", graph.traverseGraphDF('b'));
		assertEquals("h-a-c-d-r-", graph.traverseGraphDF('h'));
	}
	
	@Test
	public void inDegrees() {
		assertEquals(2, graph.inDegree('a'));
		assertEquals(0, graph.inDegree('b'));
		assertEquals(2, graph.inDegree('c'));
		assertEquals(0, graph.inDegree('h'));
		
	}
	
	@Test
	public void outDegrees() {
		assertEquals(1, graph.outDegree('a'));
		assertEquals(1, graph.outDegree('b'));
		assertEquals(1, graph.outDegree('c'));
		assertEquals(3, graph.outDegree('h'));
	}
	
	//////////////////////////////////////////
	//////////////////////////////////////////
	///////////DIJKSTRA ALGORITHM/////////////
	//////////////////////////////////////////
	//////////////////////////////////////////
	
	@Test
	public void dijkstra()
	{
		dijkstra.print();
		
		dijkstra.Dijkstra("v1");
		
	//	dijkstra.printDijkstra();
		assertArrayEquals(new double[][]{{99.0,3.0,4.0,12.0,7.0,10.0}}, dijkstra.getD());
		assertArrayEquals(new int[]{-1,0,0,5,2,4}, dijkstra.getPD());
		
		graph2.Dijkstra("v1");
	//graph2.printDijkstra();
		assertArrayEquals(new double[][]{{99,1,5,3,6}}, graph2.getD());
		assertArrayEquals(new int[]{-1,0,3,0,2}, graph2.getPD());
		
	}
	
	
	@Test
	public void printDijkstra_fail() 
	{
		try 
		{
			dijkstra.printDijkstraPath("v0", "v4");
			assertEquals(-1, dijkstra.printDijkstraPath("v9", "V4"));
		}
		catch(Exception e) 
		{
			System.out.println("Exception catched correctly");
		}
	}
	
	@Test
	public void printDikstraPath() throws Exception
	{
		dijkstra.Dijkstra("v1");
		
		assertEquals("v1v3v5v6" , dijkstra.printDijkstraPath("v1", "v6"));
		
		graph2.Dijkstra("v1");
		assertEquals("v1v4v3v5" , graph2.printDijkstraPath("v1", "v5"));
		assertEquals("v1v2" , graph2.printDijkstraPath("v1", "v2"));
		assertEquals("v1v4v3" , graph2.printDijkstraPath("v1", "v3"));
	}
	
	
	
	@Test
	public void floyd()
	{
		dijkstra.floyd(dijkstra.getSize());;
		assertArrayEquals(new double[][] {{0,3,4,12,7,10},{99,0,99,10,5,8},{99,99,0,8,3,6},
						{99,99,99,0,99,99},{99,99,99,5,0,3},{99,99,99,2,99,0}} ,
							dijkstra.getA());
		assertArrayEquals(new int[][] {{-1,-1,-1,5,2,4},{-1,-1,-1,5,-1,4},{-1,-1,-1,5,-1,4},
											{-1,-1,-1,-1,-1,-1},{-1,-1,-1,5,-1,-1},
													{-1,-1,-1,-1,-1,-1}},dijkstra.getP());
		graph2.floyd(graph2.getSize());
		assertArrayEquals(new double[][] {{0,1,5,3,6},{99,0,5,99,6},
											{99,99,0,99,1},{99,99,2,0,3},
												{99,99,99,99,0}},graph2.getA());
		assertArrayEquals(new int[][] {{-1,-1,3,-1,3},{-1,-1,-1,-1,2},
											{-1,-1,-1,-1,-1},{-1,-1,-1,-1,2}
												,{-1,-1,-1,-1,-1}}, graph2.getP());
		
	}
	
	@Test
	public void printFloydPath() throws Exception 
	{
		dijkstra.floyd(dijkstra.getSize());
		assertEquals("v1v3v5v6","v1"+ dijkstra.printFloydPath("v1", "v6")+"v6");
		assertEquals("v1v3v5v6","v1"+ dijkstra.printFloydPath("v1", "v6")+"v6");
		
		graph2.floyd(graph2.getSize());
		assertEquals("v1v4v3v5","v1"+ graph2.printFloydPath("v1","v5")+"v5");
		assertEquals("v2v3v5","v2"+ graph2.printFloydPath("v2","v5")+"v5");
		
		
		
		
	}
		
	
}
